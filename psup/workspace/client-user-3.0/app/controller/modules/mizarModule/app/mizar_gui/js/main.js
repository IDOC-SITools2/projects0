/*******************************************************************************
 * Copyright 2012-2015 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of SITools2.
 *
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SITools2. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/**
 * Configuration for require.js
 */
require.config({
    paths: {
        "jquery": "../../mizar_lite/externals/jquery/dist/jquery.min",
        "jquery.ui": "../../mizar_lite/externals/jquery-ui/jquery-ui.min",
        "underscore-min": "../../mizar_lite/externals/underscore/underscore",
        "jszip": "../../mizar_lite/externals/jszip/jszip.min",
        "saveAs": "../../mizar_lite/externals/fileSaver/FileSaver.min",
        "jquery.nicescroll.min": "../../mizar_lite/externals/jquery.nicescroll/dist/jquery.nicescroll.min",
		"string": "../../mizar_lite/externals/string/dist/string.min",        
		"fits": "../../mizar_lite/externals/fits",
        "samp": "../../mizar_lite/externals/samp",
        "gzip": "../../mizar_lite/externals/gzip",
        "crc32": "../../mizar_lite/externals/crc32",
        "deflate-js": "../../mizar_lite/externals/deflate",
        "inflate-js": "../../mizar_lite/externals/inflate",
        "wcs": "../../mizar_lite/externals/wcs",
        "jquery.ui.timepicker": "../../mizar_lite/externals/jquery.ui.timepicker/jquery.ui.timepicker",
        "gw": "../../mizar_lite/externals/GlobWeb/src",
        "jquery.once": "../../mizar_lite/externals/jquery-once/jquery.once.min",
        "selectize": "../../mizar_lite/externals/selectizejs/selectize",
        "sifter": "../../mizar_lite/externals/selectizejs/sifter",
        "microplugin": "../../mizar_lite/externals/selectizejs/microplugin",
        "flot": "../../mizar_lite/externals/flot/jquery.flot.min",
        "flot.tooltip": "../../mizar_lite/externals/flot/jquery.flot.tooltip.min",
        "flot.axislabels": "../../mizar_lite/externals/flot/jquery.flot.axislabels",
        "loadmask" : "../../mizar_lite/externals/loadmask/jquery.loadmask",

        // requirements Mizar_Lite
        "context" : "../../mizar_lite/js/context",
        "layer" : "../../mizar_lite/js/layer",
        "provider" : "../../mizar_lite/js/provider",
        "service" : "../../mizar_lite/js/service",
        "gui_core" : "../../mizar_lite/js/gui",
        "name_resolver" : "../../mizar_lite/js/name_resolver",
        "reverse_name_resolver" : "../../mizar_lite/js/reverse_name_resolver",
        "uws" : "../../mizar_lite/js/uws",
        "mizar_lite" : "../../mizar_lite",
        "service_gui" : "./service_gui",
        "uws_gui" : "./uws",
        "templates" : "../templates",
        "data" : "../data",
        "data_core" : "../../mizar_lite/data"
    },
    shim: {
        'jquery': {
            deps: [],
            exports: 'jQuery'
        },
        'jquery.ui': {
            deps: ['jquery'],
            exports: 'jQuery'
        },
        'jquery.ui.timepicker': {
            deps: ['jquery.ui'],
            exports: 'jQuery'
        },
        'underscore-min': {
            deps: ['jquery'],
            exports: '_'
        },
        'jquery.nicescroll.min': {
            deps: ['jquery'],
            exports: ''
        },
        'flot': {
            deps: ['jquery'],
            exports: '$.plot'
        },
        'flot.tooltip': {
            deps: ['flot']
        },
        'flot.axislabels': {
            deps: ['flot']
        },
        'loadmask': {
            deps: ['jquery']
        }
    },
    waitSeconds: 0
});

/**
 * Mizar widget main
 */
require(["./MizarWidget"], function (MizarWidget) {

    var mizar = new MizarWidget('#mizarWidget-div', {
        debug: false,
        navigation: {
            "initTarget": [0, 0]
        },
        positionTracker: {
            position: "bottom"
        },
        stats: {
            visible: true
        },
        sitoolsBaseUrl: 'http://sitools.akka.eu:8080',
        hipsServiceUrl: "http://aladin.unistra.fr/hips/globalhipslist?fmt=json&dataproduct_subtype=color",
        nameResolver : {
            zoomFov: 2,
            jsObject : "./name_resolver/IMCCENameResolver"
        }
        //"hipsServiceUrl": "http://aladin.unistra.fr/hips/globalhipslist?fmt=json"
    });

    // Set different GUIs
    mizar.setAngleDistanceSkyGui(true);
    mizar.setSampGui(true);
    mizar.setShortenerUrlGui(true);
    mizar.set2dMapGui(true);
    mizar.setReverseNameResolverGui(true);
    mizar.setNameResolverGui(true);
    mizar.setCategoryGui(true);
    mizar.setCompassGui(true);
    mizar.setShowCredits(false);
    mizar.setImageViewerGui(true);
    mizar.setSwitchTo2D(false);
    mizar.setExportGui(true);
    mizar.setSearchGui(false);

    mizar.viewPlanet("Mars");

    var atmosMarsLayer = {
        "category": "Other",
        "type": "atmosphere",
        "exposure": 1.4,
        "wavelength": [0.56, 0.66, 0.78],
        "name": "Atmosphere",
        "lightDir": [0, 1, 0],
        "visible": false
    };
    var coordLayer = {
        "category": "Other",
        "type": "tileWireframe",
        "name": "Coordinates Grid",
        "outline": true,
        "visible": false
    };

    var marsLayer = mizar.getLayer("Mars");
    mizar.addLayer(atmosMarsLayer, marsLayer);
    mizar.addLayer(coordLayer, marsLayer);

    var marsLayer = mizar.getLayer("Mars");
    var cratersCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Crater catalog",
        "description": "Crater catalog",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/min/costard_craters_min_3.json"
        },
        "visible": false,
        "pickable" : true        
    };
    //mizar.addLayer(cratersCatalog, marsLayer);

    var peakIsidisHellasCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Central peaks hydrated phases between Isidis and Hellas",
        "description": "Hydrated phases detected in the exhumed material by impact crater from CRISM targeted observations between Isidis and Hellas basin",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/jsonPlis/detections_crateres_benjamin_bultel_icarus.json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(peakIsidisHellasCatalog, marsLayer);

    var landingCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Landing sites",
        "description": "landing site of previous landers/rovers",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/landing_sites.json"
        },
        "visible": false,
        "pickable" : true  
    };
    mizar.addLayer(landingCatalog, marsLayer);

    var crocusCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "crocus",
        "description": "CO2 sublimation line for year 25 from OMEGA observations",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/min/crocus_ls150-200_min.json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(crocusCatalog ,marsLayer)

    var scallopedCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Scalloped depression",
        "description": "Outline of scalloped depressions from 80°E to 110°E and 40°N to 50°N using Context Camera/MRO (NASA) of 6 m/pixel and MOC/MGS (NASA) images",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/min/Scalloped_Depression_min.json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(scallopedCatalog, marsLayer);

    var hydMinCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Hydrated mineral sites",
        "description": "A global map of hydrated mineral sites at the surface of Mars detected using the reflectance data acquired by the Mars Express OMEGA hyperspectral camera and from the Mars Reconnaissance Orbiter (MRO) CRISM imaging spectrometer",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/jsonPlis/hyd_global_290615_min.json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(hydMinCatalog, marsLayer);

    var vallesMarLowCalPyroCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Valles Marineris low Calcium-Pyroxene",
        "description": "Detection of low Calcium Pyroxene in the walls of Valles Marineris Canyon based on CRISM targeted observation",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/jsonPlis/lcp_flahaut_et_al.json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(vallesMarLowCalPyroCatalog, marsLayer);

    var centralPeakMinSouthCatalog = {
        "category": "Catalogs",
        "type": "GeoJSON",
        "name": "Central peaks mineralogy south Valles Marineris",
        "description": "Mineralogy of the central peaks of impact crater derived from CRISM targeted observation south of Valles Marineris Canyon",
        "data": {
            "type": "JSON",
            "url": "http://psup.ias.u-psud.fr/sitools/upload/geojson/jsonPlis/lcp_vmwalls.json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(centralPeakMinSouthCatalog, marsLayer);


    // LAYERS FOURNIS PAR UN SERVEUR WMS
    var themisDayIr100mLayer = {
        "name": "Themis Day IR 100m",
        "type": "WMS",
        "category": "Background Layers",
        "visible": false,
        "baseUrl": "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "themis_day_ir_100m",
        "format": "image/png",
        "attribution": "Themis Day IR 100m"
    };
    mizar.addLayer(themisDayIr100mLayer,marsLayer);

    var themisNightIr100mLayer = {
        "name": "Themis Night IR 100m",
        "type": "WMS",
        "category": "Background Layers",
        "visible": false,
        "baseUrl": "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "themis_night_ir_100m",
        "format": "image/png",
        "attribution": "Themis Day IR 100m"
    };
    mizar.addLayer(themisNightIr100mLayer,marsLayer);

    var molaShadedRelierColorLayer = {
        "name": "Mars Mola Shaded Relief Color",
        "type": "WMS",
        "category": "Background Layers",
        "visible": false,
        "baseUrl": "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "mola_shaded_relief_color",
        "format": "image/png",
        "attribution": "Mars Marc Shaded Relief Color"
    };
    mizar.addLayer(molaShadedRelierColorLayer,marsLayer);

    var molaShadedRelierLayer = {
        "name": "Mars Mola Shaded Relief",
        "type": "WMS",
        "category": "Background Layers",
        "visible": false,
        "baseUrl": "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "mola_shaded_relief",
        "format": "image/png",
        "attribution": "Mars Shaded Relief "
    };
    mizar.addLayer(molaShadedRelierLayer,marsLayer);

    var tes_albedo = {
        "name": "TES Albedo",
        "type": "WMS",
        "category": "Background Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "tes_albedo",
        "description": "TES Albedo",
        "format": "image/png"
    };
    mizar.addLayer(tes_albedo,marsLayer);

    // TEST OMEGA PNG
    /*var omega_albedo_layer ={
        "name": "OMEGA ALBEDO",
        "type": "WMS",
        "category": "Background Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_albedo_r1800",
        "description": "omega albedo",
        "format": "image/png"
    };
    mizar.addLayer(omega_albedo_layer,marsLayer);*/
    var omega_solar_albedo ={
        "name": "OMEGA Solar Albedo",
        "type": "WMS",
        "category": "Background Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "OMEGA_solar_albedo",
        "description": "omega solar albedo. Reference : http://www.sciencedirect.com/science/article/pii/S0019103514005764",
        "format": "image/png",
	"attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/scolorscale_albedo.png\" /> "
    };
    mizar.addLayer(omega_solar_albedo,marsLayer);

    var omega_olivine_sp1_layer ={
        "name": "OMEGA Olivine SP1",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_olivine_sp1",
        "description": "Olivine detections using the spectral parameter SP1 (Mg-rich and/or small grain-sized (<<100 µm) and/or a low-abundance (=<10%) olivine)",
        "format": "image/png",
        "attribution": "Layer provided by IAS "

    };
    mizar.addLayer(omega_olivine_sp1_layer,marsLayer);

    var omega_olivine_sp2_layer ={
        "name": "OMEGA Olivine SP2",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_olivine_sp2",
        "description": "Olivine detections using the spectral parameter SP2 (olivine with higher iron content and/or larger grain size and/or at higher abundance than SP1)",
        "format": "image/png",
        "attribution": "Layer provided by IAS "
    };
    mizar.addLayer(omega_olivine_sp2_layer,marsLayer);

    var omega_olivine_sp3_layer ={
        "name": "OMEGA Olivine SP3",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_olivine_sp3",
        "description": "Olivine detections using the spectral parameter SP3 (Fe-rich and/or large grain size (>100 µm) and/or large abundance (>30 %))",
        "format": "image/png",
        "attribution": "Layer provided by IAS "

    };
    mizar.addLayer(omega_olivine_sp3_layer,marsLayer);

    var omega_ferric_bd530_layer ={
        "name": "OMEGA Ferric Fe3+",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_ferric_bd530",
        "description": "Ferric phases identified from Fe3+ signature at 0.53 µm",
        "format": "image/png",
        "attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/Fe3plus_scale.png\" />"

    };
    mizar.addLayer(omega_ferric_bd530_layer,marsLayer);

    var omega_ferric_nnphs_layer ={
        "name": "OMEGA Ferric Nanophase",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_ferric_nnphs",
        "description": "Nanophase ferric oxides (similar to TES dust cover)",
        "format": "image/png",
        "attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/Nano-scale.png\" />"

    };
    mizar.addLayer(omega_ferric_nnphs_layer,marsLayer);

    var omega_pyroxene_bd2000_layer ={
        "name": "OMEGA Pyroxene",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "omega_pyroxene_bd2000",
        "description": "Pyroxenes from the 2 µm band",
        "format": "image/png",
        "attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/Pyro-scale.png\" />"

    };
    mizar.addLayer(omega_pyroxene_bd2000_layer,marsLayer);

    // Autres mineral Layers
    var tes_dust_cover ={
        "name": "TES Dust Cover",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "tes_dust_cover",
        "description": "TES Dust Cover",
        "format": "image/png"
    };
    mizar.addLayer(tes_dust_cover,marsLayer);

    var tes_clinopyroxene_layer ={
        "name": "TES High-Calcium Pyroxene Abundance",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "tes_High-Ca_Pyroxene",
        "description": "TES Clinopyroxene",
        "format": "image/png"
    };
    mizar.addLayer(tes_clinopyroxene_layer,marsLayer);

    var tes_plagioclase_layer ={
        "name": "TES Plagioclase",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "tes_plagioclase",
        "description": "TES Plagioclase",
        "format": "image/png"
    };
    mizar.addLayer(tes_plagioclase_layer,marsLayer);

    var tes_low_ca_Pyroxene_layer ={
        "name": "TES Low-Ca Pyroxene",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "tes_Low-Ca_Pyroxene",
        "description": "TES Low-Ca Pyroxene",
        "format": "image/png"
    };
    mizar.addLayer(tes_low_ca_Pyroxene_layer,marsLayer);

    var tes_olivine_layer ={
        "name": "TES Olivine",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "tes_olivine",
        "description": "TES Olivine",
        "format": "image/png"
    };
    mizar.addLayer(tes_olivine_layer,marsLayer);

    var thermal_inertia ={
        "name": "OMEGA Thermal Inertia",
        "type": "WMS",
        "category": "Background Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "thermal_inertia",
        "description": "Global map of OMEGA-based thermal inertia",
        "format": "image/png",
        "attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/colorscale_ITcolor.png\" />"

    };
    mizar.addLayer(thermal_inertia,marsLayer);

    var water_weight ={
        "name": "OMEGA Surface hydration",
        "type": "WMS",
        "category": "Mineral Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "water_weight",
        "description": "Global distribution of the surface hydration from the 3 µm band as seen by OMEGA",
        "format": "image/png",
	"attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/WP_color_vertical.png\" />"

    };
    mizar.addLayer(water_weight,marsLayer);

    var surface_emissivity ={
        "name": "OMEGA Surface Emissivity at 5µm",
        "type": "WMS",
        "category": "Background Layers",
        "baseUrl":  "http://idoc-wmsmars.ias.u-psud.fr/cgi-bin/mapserv?map=/home/cnes/mars/mars.map",
        "layers": "surface_emissivity",
        "description": "Global map of OMEGA-based surface emissivity at 5µm",
        "format": "image/png",
        "attribution": "Layer provided by IAS <br><img width='300' height='50' src=\"http://psup.ias.u-psud.fr/sitools/upload/colorscale_emi_color.png\" />"

    };
    mizar.addLayer(surface_emissivity,marsLayer);

    var emars_crism  = {
        "category": "MarsSI Data",
        "type": "GeoJSON",
        "name": "emars_crism",
        "description": "CRISM data cubes generated by MarSI requests",
        "data": {
            "type": "JSON",
            "url": "http://emars.univ-lyon1.fr/geoserver/EmarsGis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=EmarsGis:emars_crism&maxFeatures=5000&outputFormat=application/json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(emars_crism, marsLayer);

    var emars_ctx  = {
        "category": "MarsSI Data",
        "type": "GeoJSON",
        "name": "emars_ctx",
        "description": "CTX images generated by MarSI requests",
        "data": {
            "type": "JSON",
            "url": "http://emars.univ-lyon1.fr/geoserver/EmarsGis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=EmarsGis:emars_ctx&maxFeatures=5000&outputFormat=application/json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(emars_ctx, marsLayer);

    var emars_ctx_dtm  = {
        "category": "MarsSI Data",
        "type": "GeoJSON",
        "name": "emars_ctx_dtm",
        "description": "CTX DTM generated by MarSI requests",
        "data": {
            "type": "JSON",
            "url": "http://emars.univ-lyon1.fr/geoserver/EmarsGis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=EmarsGis:emars_ctx_dtm&maxFeatures=5000&outputFormat=application/json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(emars_ctx_dtm, marsLayer);

    var emars_hirise  = {
        "category": "MarsSI Data",
        "type": "GeoJSON",
        "name": "emars_hirise",
        "description": "HiRISE images generated by MarSI requests",
        "data": {
            "type": "JSON",
            "url": "http://emars.univ-lyon1.fr/geoserver/EmarsGis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=EmarsGis:emars_hirise&maxFeatures=5000&outputFormat=application/json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(emars_hirise, marsLayer);

    var emars_hirise_dtm  = {
        "category": "MarsSI Data",
        "type": "GeoJSON",
        "name": "emars_hirise_dtm",
        "description": "HiRISE DTM generated by MarSI requests",
        "data": {
            "type": "JSON",
            "url": "http://emars.univ-lyon1.fr/geoserver/EmarsGis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=EmarsGis:emars_hirise_dtm&maxFeatures=5000&outputFormat=application/json"
        },
        "visible": false,
        "pickable" : true
    };
    mizar.addLayer(emars_hirise_dtm, marsLayer);

});
