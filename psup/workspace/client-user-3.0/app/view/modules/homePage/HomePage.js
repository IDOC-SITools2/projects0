/*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 * 
 * This file is part of SITools2.
 * 
 * SITools2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SITools2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SITools2. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/*global Ext, sitools, i18n, projectGlobal, alertFailure, showResponse*/

Ext.namespace('sitools.user.view.modules.homePage');
/**
 * ProjectDescription Module
 * @class sitools.user.modules.projectDescription
 * @extends Ext.Panel
 */
Ext.define('sitools.user.view.modules.homePage.HomePage', {
   
    extend : 'Ext.panel.Panel',

    border : false,
    bodyBorder : false,
    layout: 'border',
    activeNode : null,
    initComponent : function () {
	console.log("Je suis dans la view");
        this.url = "http://psup.ias.u-psud.fr/sitools/common/html/home/HomePSUP.html";

        var htmlReaderCfg = {
               defaults : {
                   padding : 10
               },
               layout : 'fit',
               region : 'center',
               src : this.url
        };

        this.htmlReader = Ext.create('Ext.ux.IFrame', htmlReaderCfg);
        this.items = [ this.htmlReader ];

        this.callParent(arguments);

    }, 
    /**
     * method called when trying to save preference
     * 
     * @returns
     */
    _getSettings : function () {
        return {
            preferencesPath : "/modules",
            preferencesFileName : this.id
        };

    }
});
