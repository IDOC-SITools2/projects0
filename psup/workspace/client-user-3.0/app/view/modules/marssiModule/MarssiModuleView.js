/***************************************
* Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
* 
* This file is part of SITools2.
* 
* SITools2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* SITools2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
***************************************/
/*global Ext, sitools, i18n, document, projectGlobal, SitoolsDesk, userLogin, DEFAULT_PREFERENCES_FOLDER, loadUrl*/


Ext.namespace('sitools.user.view.modules.marssiModule');


Ext.define('sitools.user.view.modules.marssiModule.MarssiModuleView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.marssiModuleView',
	layout : 'fit',
	
	initComponent : function () {
		
		var urlMarssi = "Https://emars.univ-lyon1.fr/MarsSI";
		this.items = [ { layout: 'fit',
                        region : 'center',
                        autoEl: { tag: 'iframe',
				  src: urlMarssi
                        },
                        xtype: 'box'}
                ];
		
	        this.callParent(arguments);	

	},

	/**
        * method called when trying to save preference
	* 
	* @returns
	*/
	_getSettings : function () {
        	return {
        		preferencesPath : "/modules",
	        	preferencesFileName : this.id,
        		xtype : this.$className
		};

	}

});

