Ext.namespace('sitools.user.modules');

Ext.define('sitools.user.modules.MarssiModule', {
	extend : 'sitools.user.core.Module',

	controllers : [],

	init : function () {
        	var view = Ext.create('sitools.user.view.modules.marssiModule.MarssiModuleView', {
   //         		moduleModel : this.getModuleModel()
        	});
		
        	this.show(view);

        	this.callParent(arguments);
    	},

     /**
     * method called when trying to save preference
     * 
     * @returns
     */
    _getSettings : function () {
        return {
            preferencesPath : "/modules",
            preferencesFileName : this.id
        };

    }

});

