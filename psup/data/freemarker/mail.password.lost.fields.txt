# ---------------
# fr.cnes.sitools.mail.model.Mail mail
${mail.serialVersionUID}
${mail.subject}
${mail.from}
${mail.body}
${mail.sentDate}
${mail.toList}
${mail.ccList}
${mail.bccList}
# ---------------
# java.lang.String adminMail
${adminMail.value}
${adminMail.offset}
${adminMail.count}
${adminMail.hash}
${adminMail.serialVersionUID}
${adminMail.serialPersistentFields}
${adminMail.CASE_INSENSITIVE_ORDER}
# ---------------
# java.lang.String sitoolsUrl
${sitoolsUrl.value}
${sitoolsUrl.offset}
${sitoolsUrl.count}
${sitoolsUrl.hash}
${sitoolsUrl.serialVersionUID}
${sitoolsUrl.serialPersistentFields}
${sitoolsUrl.CASE_INSENSITIVE_ORDER}
# ---------------
# fr.cnes.sitools.security.model.User user
${user.serialVersionUID}
${user.identifier}
${user.firstName}
${user.lastName}
${user.email}
${user.properties}
${user.secret}
# ---------------
# java.lang.String passwordLostUrl
${passwordLostUrl.value}
${passwordLostUrl.offset}
${passwordLostUrl.count}
${passwordLostUrl.hash}
${passwordLostUrl.serialVersionUID}
${passwordLostUrl.serialPersistentFields}
${passwordLostUrl.CASE_INSENSITIVE_ORDER}
