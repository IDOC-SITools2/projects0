/***************************************
* Copyright 2010-2013 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
* 
* This file is part of SITools2.
* 
* SITools2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* SITools2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
***************************************/
/*global Ext, sitools, projectGlobal, commonTreeUtils, showResponse, document, i18n, loadUrl, SITOOLS_DEFAULT_PROJECT_IMAGE_URL, SitoolsDesk*/
/*
 * @include "../../env.js" 
 */
Ext.namespace('sitools.user.modules');

/**
 * Dataset Explorer Module.
 * Displays each dataset of the Project.
 * @class sitools.user.modules.datasetExplorerDataView
 * @extends Ext.tree.TreePanel
 */
sitools.user.modules.projectTimeline_byday = Ext.extend(Ext.Panel, {
	layout : "border",
	
	initComponent : function () {
		
		var myDataView = new Ext.DataView({
    		id : "line_byday",
            region : 'center',
            singleSelect : true,
            autoScroll : true,
	    });
    	this.items = [myDataView];
    	var projectAttachment = projectGlobal.sitoolsAttachementForUsers;
    	Ext.Ajax.request( {
    		url : '../js/modules/projectTimeline/timeline_picard.json',
    		method : "GET",
			success : function(response, opts) {
				var activityList = new Array();
				var data= Ext.decode(response.responseText);
				var i=0;
				var nb=15;
				var selecteddata=null;
				var actdata=null;
				Ext.DomHelper.append(Ext.get('line_byday'),"<div id='searchbox'/>");
				Ext.DomHelper.append(Ext.get('searchbox'),"<div id='startbox'/>");
				Ext.DomHelper.append(Ext.get('startbox')," From <input type='text' id='start'/> ");
				Ext.DomHelper.append(Ext.get('searchbox'),"<div id='endbox'/>");
				Ext.DomHelper.append(Ext.get('endbox')," To <input type='text' id='end'/> ");
				Ext.DomHelper.append(Ext.get('searchbox')," <button id='search' type='button'> GO </button> ");
				Ext.DomHelper.append(Ext.get('searchbox'),"<div id='data'/>");
				Ext.DomHelper.append(Ext.get('data'),"<button id='all' >Show all the activities</button>");
				Ext.DomHelper.append(Ext.get('data'),"<button id='withdata'>Show the activities with data</button>");
				Ext.get('all').on("click", function () {
						var start=document.getElementById('start').value;
						var end=document.getElementById('end').value;
						actdata=false;
						select(start,end,actdata);
			    });
				Ext.get('withdata').on("click", function () {
						var start=document.getElementById('start').value;
						var end=document.getElementById('end').value;
						actdata=true;
						select(start,end,actdata);
			    });
				Ext.get('search').on("click", function () {
					var start=document.getElementById('start').value;
					var end=document.getElementById('end').value;
					select(start,end);
			    });
				
    			i=getactivity(i,nb);
				sitools.user.modules.timeline.prepareTimeline('line_byday',activityList);
    			activityList = new Array();
				var appendTo = Ext.get('timeline');
				Ext.get(Ext.DomHelper.append(appendTo,"<button id='more' type='button'> more </button>"));
				var more=Ext.get('more');
				more.on("click", function () {
					if(selecteddata==null){
						i=getactivity(i,nb);
						sitools.user.modules.timeline.addTimeline(activityList);
						activityList = new Array();
					}
					else {
						i=getactivity(i,nb,selecteddata);
						sitools.user.modules.timeline.addTimeline(activityList);
						activityList = new Array();
					}
			    });
				
				function select(start,end){
					var actdata=arguments[2]?arguments[2]:false;
					var i=0;
					activityList = new Array();
					Ext.get('timeline').remove();
					var selecteddata=selectdate(start,end);
					i=getactivity(i,nb,selecteddata,actdata);
					console.log(activityList);
					sitools.user.modules.timeline.prepareTimeline('line_byday',activityList);
					activityList = new Array();
					var appendTo = Ext.get('timeline');
					Ext.get(Ext.DomHelper.append(appendTo,"<button id='more' type='button'> more </button>"));
					more=Ext.get('more');
					more.on("click", function () {
						if(selecteddata==null) {
							i=getactivity(i,nb);
						}
						else {
							if(actdata){
								i=getactivity(i,nb,selecteddata,actdata);
								sitools.user.modules.timeline.addTimeline(activityList);
								activityList = new Array();
							}else{
								i=getactivity(i,nb,selecteddata);
								sitools.user.modules.timeline.addTimeline(activityList);
								activityList = new Array();
							}
						}
				    });
				}
				function getactivity(i,nb){
					var selected=arguments[2]?arguments[2]:data.timeline;
					var actdata=arguments[3]?arguments[3]:false;
					var count=0;
					while(selected[i]){
						if((selected[i].data&&actdata)||!actdata){
							var activity=new sitools.user.modules.timeline.createActivity(selected[i]);
							if(activityList[0]){
								if(activity.getDayCount()!=activityList[activityList.length-1].getDayCount()){
									if(count==nb-1){
										return i;
									}else{
										activityList.push(activity);
										i++;
										count++;
									}
								}else{
									activityList.push(activity);
									i++;
								}
							}else{
								activityList.push(activity);
								i++;
							}
						}else{
							i++;
						}
					}
					return i;
				}
				
				function selectdate(startdate,enddate){
					var start=null;
					var end=null;
					if(startdate==''&&enddate==''){
						return data.timeline;
					}
					for(var n=0;n<data.timeline.length;n++){
						if(start!=null&&end!=null) {break;}
						if(data.timeline[n].date>=startdate){
							if(n==0){
								start=n;
							}else{
								if(data.timeline[n-1].date<startdate)
									start=n;
							}
						}
						if(data.timeline[n].date<=enddate){
							if(n==data.timeline.length-1){
								end=n;
							}else{
								if(data.timeline[n+1].date>enddate){
									end=n;
								}
							}
						}
					}
					if(start==null||end==null){
						alert('Wrong date!');
						return data.timeline;
					}
					if(start>end){
						alert('Wrong date!');
						return data.timeline;
					}
					//console.log(data.timeline.slice(start,end+1));
					return data.timeline.slice(start,end+1);
					
				}
			} , 
			failure : function(response, opts) {          
				alert("Error while loading data : "+response.responseText);                  
			}
		});	
    
		sitools.user.modules.projectTimeline_byday.superclass.initComponent.call(this);
	},
	
    afterRender : function () {
    	sitools.user.modules.projectTimeline_byday.superclass.afterRender.apply(this, arguments);
        
    },
    /**
     * method called when trying to save preference
     * @returns
     */
    _getSettings : function () {
		return {
            preferencesPath : "/modules", 
            preferencesFileName : this.id
        };

    }
    
});
Ext.reg('sitools.user.modules.projectTimeline_byday', sitools.user.modules.projectTimeline_byday);






