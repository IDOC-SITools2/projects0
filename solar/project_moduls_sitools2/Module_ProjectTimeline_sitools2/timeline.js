Ext.namespace('sitools.user.modules.timeline');
sitools.user.modules.timeline.prepareTimeline= function (s,list){
	var appendTo = Ext.get(s);
	var container = Ext.get(Ext.DomHelper.append(appendTo,'<div id="timeline" align="center" class="timeline"></div>'));
	container=Ext.get(Ext.DomHelper.append(container,"<ul id='lineul' class='timeline'/>"));
	sitools.user.modules.timeline.showActivity(list,container);
}
sitools.user.modules.timeline.addTimeline=function (list){
	var container = Ext.get('lineul');
	sitools.user.modules.timeline.showActivity(list,container);
}
sitools.user.modules.timeline.createActivity= function (activity){
	this.daynum=activity.daynum;
	this.text=activity.text;
	this.date=activity.date;
	this.day=activity.day;
	this.dayCount=activity.daycount;
	this.startTime=activity.starttime;
	this.endTime=activity.endtime;
	this.havedata=activity.data;
	this.dataseturl=activity.dataseturl;
	this.getDaynum=getDaynum;
	function getDaynum(){
		return this.daynum;
	}
	this.getURL=getURL;
	function getURL(){
		return this.dataseturl;
	}
	this.haveData=haveData;
	function haveData(){
		//alert(this.havedata);
		return this.havedata;
	}
	this.getText=getText;
	function getText(){
		return this.text;
	}
	this.getDate=getDate;
	function getDate(){
		return this.date;
	}
	this.getDay=getDay;
	function getDay(){
		return this.day;
	}
	this.getDayCount=getDayCount;
	function getDayCount(){
		return this.dayCount;
	}
	this.getStartTime=getStartTime;
	function getStartTime(){
		return this.startTime;
	}
	this.getEndTime=getEndTime;
	function getEndTime(){
		return this.endTime;
	}
}

sitools.user.modules.timeline.showActivity= function (activityList, ulObj){
	var count=0;
	//var value='/sodism/imagessingulieres/dco';
	 for(var i=0; i<activityList.length;i++){
		 var newactivitynodata="<pre class='nodata' style='height:"+((activityList[i].getEndTime().split(':')[0]-activityList[i].getStartTime().split(':')[0])*15-2)+"px;'><div class='activity_name'>"+activityList[i].getText()+/*" "+activityList[i].getStartTime()+"-"+activityList[i].getEndTime()+*/"</div></pre>";
		 var newactivitydata="<pre class='havedata' style='height:"+((activityList[i].getEndTime().split(':')[0]-activityList[i].getStartTime().split(':')[0])*15-2)+"px;'"
		 					 +"onClick='sitools.user.clickDatasetIcone(\"/picard\", \"search\",\""+activityList[i].getDate()+"\"+\"*\"+\""+activityList[i].getStartTime()+"\"+\"*\"+\""+activityList[i].getEndTime()+"\"); return false;'><div class='activity_name'>"
		 					 +activityList[i].getText()+/*" "+activityList[i].getStartTime()+"-"+activityList[i].getEndTime()
		 					 +*/"<div class='getdata'><img src='" + loadUrl.get('APP_URL')
		 					 + "/common/res/images/icons/32x32/tree_datasets_32'></div>"
		 					 /*+"<div class='getdata'><a href='#' onClick='sitools.user.clickDatasetIcone(\""+activityList[i].getURL()+"\", \"data\"); return false;'>"
	                         +"<img class='icongetdata' src='" +loadUrl.get('APP_URL')
	                         + "/common/res/images/icons/32x32/tree_datasets_32.png'>"
	                         +"</a></div>" */
	                         +"</div></pre>";
		 var ruler1="<div class='ruler ruler1'>00:00:00</div>";
		 var ruler2="<div class='ruler ruler2'>06:00:00</div>";
		 var ruler3="<div class='ruler ruler3'>12:00:00</div>";
		 var ruler4="<div class='ruler ruler4'>18:00:00</div>";
		 var ruler5="<div class='ruler ruler5'>24:00:00</div>";
			 if(i==0 || activityList[i].getDayCount()!=activityList[i-1].getDayCount()){
				 count++;
				 //if(count%2!=0){
					 if(activityList[i].getDayCount()==activityList[activityList.length-1].getDayCount()) {var liObj = Ext.DomHelper.append(ulObj, "<li style='margin-left:"+(-1136+(count-1)*224)+"px;' class='lastday'/>");}
				 	else {var liObj=Ext.DomHelper.append(ulObj, "<li class='day'/>");}
				 //}else{
					// var liObj = Ext.DomHelper.append(ulObj, "<li class='alt day'/>");
				 //}
				 Ext.DomHelper.append(liObj, 
						"<div class='time'>"+activityList[i].getDay()+"</div>"
						+"<div class='date'>"+activityList[i].getDate()+"</div>"
						+"<div class='number'>"+activityList[i].getDayCount()+"</div>"
						//+ruler1
						/*+ruler2+ruler3+ruler4+ruler5*/
						);
				 var contentObj=Ext.DomHelper.append(liObj,"<div class='content'/>");
				 if(activityList[i].getStartTime().split(':')[0]!=0)
					 Ext.DomHelper.append(contentObj,"<pre style='height:"+((activityList[i].getStartTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
			
				
			 }
			 else{
				 if(activityList[i].getStartTime().split(':')[0]!=activityList[i-1].getEndTime().split(':')[0])
					 Ext.DomHelper.append(contentObj,"<pre style='height:"+((activityList[i].getStartTime().split(':')[0]-activityList[i-1].getEndTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
			 } 
			 //Ext.DomHelper.append(contentObj,newactivity);
			 if(activityList[i].haveData()){
				 //alert("hasdata");
				 Ext.DomHelper.append(contentObj,newactivitydata);
			 }else{
				 //alert("nodata");
				 Ext.DomHelper.append(contentObj,newactivitynodata);
			 }
			 if(i==activityList.length-1||activityList[i].getDayCount()!=activityList[i+1].getDayCount()){
				/* if(activityList[i].getEndTime().split(':')[0]<=6){
					 Ext.DomHelper.append(liObj,ruler2);
					 if(activityList[i].getEndTime().split(':')[0]!=6){
						 Ext.DomHelper.append(contentObj,"<pre style='height:"+((6-activityList[i].getEndTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
					 }
				 }
				 if(activityList[i].getEndTime().split(':')[0]<=12&&activityList[i].getEndTime().split(':')[0]>6){
					 Ext.DomHelper.append(liObj,ruler2+ruler3);
					 if(activityList[i].getEndTime().split(':')[0]!=12){
						 Ext.DomHelper.append(contentObj,"<pre style='height:"+((12-activityList[i].getEndTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
					 }
				 }
				 if(activityList[i].getEndTime().split(':')[0]<=18&&activityList[i].getEndTime().split(':')[0]>12){
					 Ext.DomHelper.append(liObj,ruler2+ruler3+ruler4);
					 if(activityList[i].getEndTime().split(':')[0]!=18){
						 Ext.DomHelper.append(contentObj,"<pre style='height:"+((18-activityList[i].getEndTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
					 }
				 }
				 if(activityList[i].getEndTime().split(':')[0]<=24&&activityList[i].getEndTime().split(':')[0]>18){
					 Ext.DomHelper.append(liObj,ruler2+ruler3+ruler4+ruler5);
					 if(activityList[i].getEndTime().split(':')[0]!=24){
						 Ext.DomHelper.append(contentObj,"<pre style='height:"+((24-activityList[i].getEndTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
					 }
				 }*/
					 
				 if(activityList[i].getEndTime().split(':')[0]!=24){
					 Ext.DomHelper.append(contentObj,"<pre style='height:"+((24-activityList[i].getEndTime().split(':')[0])*15-2)+"px;background-color: #D8D8D8;'></pre>");
				 }
					 
			 }
	 }
}
