Ext.namespace('sitools.user.modules.weekTimeline');
sitools.user.modules.weekTimeline.createWeek= function (list){
	var date=new Date(list[0].date);
	switch(list[0].day){
		case "Monday":
			this.firstday=list[0].daynum;
			this.firstdate=date;
	        break;
	    case "Tuesday":
	    	this.firstday=list[0].daynum-1.0;
	    	this.firstdate=new Date(date-24*60*60*1000);
	        break;
	    case "Wednesday":
	    	this.firstday=list[0].daynum-2.0;
	    	this.firstdate=new Date(date-2*24*60*60*1000);
	        break;
	    case "Thursday":
	    	this.firstday=list[0].daynum-3.0;
	    	this.firstdate=new Date(date-3*24*60*60*1000);
	        break;
	    case "Friday":
	    	this.firstday=list[0].daynum-4.0;
	    	this.firstdate=new Date(date-4*24*60*60*1000);
	        break;
	    case "Saturday":
	    	this.firstday=list[0].daynum-5.0;
	    	this.firstdate=new Date(date-5*24*60*60*1000);
	        break;
	    case "Sunday":
	    	this.firstday=list[0].daynum-6.0;
	    	this.firstdate=new Date(date-6*24*60*60*1000);
	        break;
	}
	this.lastday=this.firstday+6.0;
	this.lastdate=new Date();
	this.lastdate.setFullYear(this.firstdate.getFullYear());
	this.lastdate.setMonth(this.firstdate.getMonth());
	this.lastdate.setDate(this.firstdate.getDate()+6);
	this.text="";
	this.activities=new Array();
	for(var i=0;i<list.length;i++){
		this.activities.push(new sitools.user.modules.timeline.createActivity(list[i]));
		if(i==0){
			this.text=this.text+list[i].text+"<br/>";
		}else{
			if(list[i].text!=list[i-1].text){
				this.text=this.text+list[i].text+"<br/>";
			}
		}
		
	}
	this.getFirstday=getFirstday;
	function getFirstday(){
		return this.firstday;
	}
	this.getLastday=getLastday;
	function getLastday(){
		return this.lastday;
	}
	this.getFirstdate=getFirstdate;
	function getFirstdate(){
		return this.firstdate;
	}
	this.getLastdate=getLastdate;
	function getLastdate(){
		return this.lastdate;
	}
	this.getText=getText;
	function getText(){
		return this.text;
	}
	this.getActivities=getActivities;
	function getActivities(){
		return this.activities;
	}
}
sitools.user.modules.weekTimeline.prepareWeekTimeline= function (s,list){
	var appendTo = Ext.get(s);
	var container = Ext.get(Ext.DomHelper.append(appendTo,'<div id="timeline" align="center" class="timeline"></div>'));
	container=Ext.get(Ext.DomHelper.append(container,"<ul id='lineul' class='timeline'/>"));
	sitools.user.modules.weekTimeline.showActivity(list,container);
}
sitools.user.modules.weekTimeline.addWeekTimeline=function (list){
	var container = Ext.get('lineul');
	sitools.user.modules.weekTimeline.showActivity(list,container);
}
sitools.user.modules.weekTimeline.showActivity= function (weekList, ulObj){
	var count=0;
	for(var i=0; i<weekList.length;i++){
		if(count%2!=1){
			var liObj = Ext.DomHelper.append(ulObj, "<li/>");
		}else{
			var liObj = Ext.DomHelper.append(ulObj, "<li class='alt'/>");
		}
		var monday=weekList[i].getFirstdate().toDateString().split(" ");
		var sunday=weekList[i].getLastdate().toDateString().split(" ");
		Ext.DomHelper.append(liObj, 
				"<div class='monday'>"+monday[1]+" "+monday[2]+" "+monday[3]+" -</div>"
				+"<div class='sunday'>- "+sunday[1]+" "+sunday[2]+" "+sunday[3]+"</div>"
				+"<div class='weeknumber'><strong>W "+(weekList[i].getFirstday()-40343.0)/7+"</strong></div>"
				);
		var contentObj=Ext.DomHelper.append(liObj,"<div class='preview' />");
		var activities=weekList[i].getActivities();
		var preObj=Ext.DomHelper.append(contentObj,"<pre class='nodata' onclick='sitools.user.modules.weekTimeline.viewDetail(this.parentNode);' style='height:200px;'></pre>");
		var preObj=Ext.DomHelper.append(preObj,"<div class='activity_name'></div>");
		for(var j=0;j<activities.length;j++){
			if(j==0){
				if(activities[j].haveData()){
					Ext.DomHelper.append(preObj,"<div style='color:#F7FE2E'>"+activities[j].getText()+"</div>");
				}else{
					Ext.DomHelper.append(preObj,"<div>"+activities[j].getText()+"</div>");
				}
			}else{
				if(activities[j].getText()!=activities[j-1].getText()){
					if(activities[j].haveData()){
						Ext.DomHelper.append(preObj,"<div style='color:#F7FE2E'>"+activities[j].getText()+"</div>");
					}else{
						Ext.DomHelper.append(preObj,"<div>"+activities[j].getText()+"</div>");
					}
				}
			}
		}
		Ext.DomHelper.append(contentObj,"<div class='detail_button' onclick='sitools.user.modules.weekTimeline.viewDetail(this.parentNode);'><div class='img_detail'></div></div>");
		var detailObj=Ext.DomHelper.append(liObj,"<div class='detail' style='display:none;' />");
		var detailUlObj=Ext.get(Ext.DomHelper.append(detailObj,"<ul class='timeline' />"));
		 var ruler1="<div class='ruler ruler1'>00:00:00</div>";
                 var ruler2="<div class='ruler ruler2'>06:00:00</div>";
                 var ruler3="<div class='ruler ruler3'>12:00:00</div>";
                 var ruler4="<div class='ruler ruler4'>18:00:00</div>";
                 var ruler5="<div class='ruler ruler5'>24:00:00</div>";
		Ext.DomHelper.append(detailUlObj,ruler1);
		Ext.DomHelper.append(detailUlObj,ruler2);
		Ext.DomHelper.append(detailUlObj,ruler3);
		Ext.DomHelper.append(detailUlObj,ruler4);
		Ext.DomHelper.append(detailUlObj,ruler5);
		sitools.user.modules.timeline.showActivity(weekList[i].getActivities(),detailUlObj);
		Ext.DomHelper.append(detailObj,"<div class='close_button' onclick='sitools.user.modules.weekTimeline.backtoweek(this.parentNode);'><div class='img_close'></div></div>");
		
		count++;
		
	}
}

sitools.user.modules.weekTimeline.back=function(o){
	o.parentNode.parentNode.scrollIntoView();
	
}

sitools.user.modules.weekTimeline.viewDetail=function(o){
	//Ext.get(o).hide();
	o.style.display="none";
	//o.nextSibling.style.display="block";
	Ext.get(o.nextSibling).slideIn('t', {duration: 0.5 });
	
}
sitools.user.modules.weekTimeline.backtoweek=function(o){
	//Ext.get(o).slideOut('t',{duration: 0.5});
	o.style.display="none";	
	//o.previousSibling.style.display="block";
	Ext.get(o.previousSibling).slideIn('t', {duration: 0.3 });
}





































