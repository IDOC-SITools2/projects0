/**
 * Converted into Ext JS by : Shariq Shaikh
 *
 * http://twitter.com/shaikhmshariq
 * Adopted from jQuery org-chart/tree plugin (https://github.com/wesnolte/ExtJSOrgChart).
 *
 * Author: Wes Nolte
 * http://twitter.com/wesnolte 
 *
 * Based on the work of Mark Lee
 * http://www.capricasoftware.co.uk 
 *
 * Copyright (c) 2011 Wesley Nolte
 * Dual licensed under the MIT and GPL licenses.
 *
 */
Ext.namespace('sitools.user.modules.ExtJSOrgChart');
//structure to hold node details 
sitools.user.modules.ExtJSOrgChart.createNode = function (node){
	//this.parentId=parentId;
	//this.id=id;
	this.text=node.text;
	this.id=node.datasetId;
	this.type=node.type;
	this.url=node.url;
	this.Nb=node.nbRecord;
	this.description=node.description;
	if (!Ext.isEmpty(node.imageDs)) {
		this.image=node.imageDs;
	} else if (!Ext.isEmpty(node.image)){
		this.image=node.image.url;
	} else {
		this.image=SITOOLS_DEFAULT_PROJECT_IMAGE_URL;
	}
	this.child= new Array();
	this.getImage=getImage;
	function getImage(){
		return this.image;
	}
	this.getText=getText;
	function getText(){
		return this.text;
	}
	this.getUrl=getUrl;
	function getUrl(){
		return this.url;
	}
	this.getID=getID;
	function getID(){
		return this.id;
	}
	this.getType=getType;
	function getType(){
		return this.type;
	}
	this.getNb=getNb;
	function getNb(){
		return this.Nb;
	}
	this.getDes=getDes;
	function getDes(){
		return this.description;
	}
	this.getChildNodes=getChildNodes;
	function getChildNodes(){
		return this.child;
	}
	this.hasChildNodes=hasChildNodes;
	function hasChildNodes(){
		return this.child.length > 0;
	}
	this.addChild=addChild;
	function addChild(childElem){
		this.child.push(childElem);
		return this;
	}
	this.setChildNodes=setChildNodes;
	function setChildNodes(child){
		this.child=child;
	}
}
sitools.user.modules.ExtJSOrgChart.opts=null;
sitools.user.modules.ExtJSOrgChart.buildNode= function (node, appendTo, level, opts) {
	//sitools.user.modules.ExtJSOrgChart.opts=opts;
	//console.log(sitools.user.modules.ExtJSOrgChart.opts);
    var tableObj = Ext.DomHelper.append(appendTo, "<table cellpadding='0' cellspacing='0' border='1'/>");
    var tbodyObj = Ext.DomHelper.append(tableObj, "<tbody/>");

    // Construct the node container(s)
    var nodeRow = Ext.get(Ext.DomHelper.append(tbodyObj, "<tr/>")).addClass("node-cells");
    var nodeCell = Ext.get(Ext.DomHelper.append(nodeRow, "<td colspan='2' />")).addClass("node-cell");
	
    var childNodes = node.getChildNodes();
    var nodeDiv;
    var showStr="";
    if (childNodes.length > 1) {
        nodeCell.dom.setAttribute('colspan', childNodes.length * 2);
    }
    // Draw the node
    if(node.getType()=='node'&&level == 0){
    	var nodeContent='<div class="notdatasettop">'+node.getText()+'</div>';
    }else if(node.getType()=='node'&&level >= 1){
	if(node.getImage()&&node.getText().indexOf("Download")!=-1){
		var nodeContent='<img class="level1img" src="'+node.getImage()+'"/>';
        	nodeContent+='<div class="notdataset">'+node.getText()+'</div>';	
	}else{
		var nodeContent = '<div class="notdataset">'+node.getText()+'</div>';
	}
    }else{
    	//alert(node.getID());
    	var nodeContent=
    	/*	'<li id="'+node.getID()+'">'+
            '<strong>'+node.getText()+'</strong>'+
            '<span>'+node.getNb()+'</span><br/>'+     
            '<div class="dataset_services">'+
            "<a href='#' onClick='sitools.user.clickDatasetIcone(\"" + node.getUrl()
                + "\", \"data\"); return false;'><img src='" + loadUrl.get('APP_URL')
                + "/common/res/images/icons/32x32/tree_datasets_32.png'></a>"+
               
            '</div>'+
            '<div class="dataset_description">'+node.getDes()+'</div>'+
        '</li>';*/
    	"<div class='leaf'>"
    	+"<div class='pictitle'>"	
    	+"<img class='datasetimg' onClick='sitools.user.clickDatasetIcone(\"" + node.getUrl()
    	        + "\", \"data\"); return false;' width='80' height='80' src='"+node.getImage()+"' />"
        +'<div class="datasettitle">'+node.getText().split(' -')[2]+'</div>'
        +"</div>"
        +"<br/><a href='#' onClick='sitools.user.clickDatasetIcone(\"" + node.getUrl()
        + "\", \"data\"); return false;'><img src='" + loadUrl.get('APP_URL')
        + "/common/res/images/icons/32x32/tree_datasets_32.png'></a>"
        +'<br/><span class="nbrecords">('+node.getNb()+' records)</span><br/>'
        /*+"<a href='#' onClick='return SitoolsDesk.navProfile.manageDatasetViewAlbumIconForm(\"" + node.getUrl() + "\");'>query form</a>"*/
        +"<br/><a  href='#' class='align-right' ext:qtip='Description' onClick='sitools.user.clickDatasetIcone(\"" + node.getUrl()
        + "\", \"desc\"); return false;'>Description</a>"
        +"</div>";
    }
    if(childNodes.length>0){
    //	nodeContent += "<div class='img_detail' onClick='sitools.user.modules.ExtJSOrgChart.ss(childNodes,\"\",0,opts,tbodyObj)'></div>";
    }
    
    nodeDiv = Ext.get(Ext.DomHelper.append(nodeCell,"<div>")).addClass("node");
	
    nodeDiv.dom.innerHTML=nodeContent;
    if (childNodes.length > 0) {
        // recurse until leaves found (-1) or to the level specified
        if (opts.depth == -1 || (level + 1 < opts.depth)) {
            var downLineRow = Ext.DomHelper.append(tbodyObj,"<tr class='"+showStr+"' />");
            var downLineCell = Ext.DomHelper.append(downLineRow,"<td/>");
			downLineCell.setAttribute('colspan',childNodes.length * 2);
            // draw the connecting line from the parent node to the horizontal line 
            downLine = Ext.get(Ext.DomHelper.append(downLineCell,"<div></div>")).addClass("line down");
	
			// Draw the horizontal lines
            var linesRow = Ext.DomHelper.append(tbodyObj,"<tr class='"+showStr+"' />");
            Ext.each(childNodes,function (item,index) {
                var left = Ext.get(Ext.DomHelper.append(linesRow,"<td>&nbsp;</td>")).addClass("line left top");
                var right = Ext.get(Ext.DomHelper.append(linesRow,"<td>&nbsp;</td>")).addClass("line right top");
            });

            // horizontal line shouldn't extend beyond the first and last child branches
            Ext.select("td:first",false,linesRow).removeClass("top");
            Ext.select("td:last",false,linesRow).removeClass("top");
                

            var childNodesRow = Ext.DomHelper.append(tbodyObj,"<tr class='"+showStr+"' />");
            Ext.each(childNodes,function (item,index) {
                var td = Ext.DomHelper.append(childNodesRow,"<td class='node-container'/>");
				td.setAttribute('colspan',2);
                // recurse through children lists and items
				sitools.user.modules.ExtJSOrgChart.buildNode(item, td, level + 1, opts);
            });

        }
    }


    /* Prevent trees collapsing if a link inside a node is clicked */
    Ext.each(Ext.select('a',true,nodeDiv.dom),function(item,index){
		item.onClick= function(e){
			console.log(e);
			e.stopPropagation();
		}
	});
}
sitools.user.modules.ExtJSOrgChart.ss= function(childNodes,showStr,level,opts,tbodyObj){
    if (childNodes.length > 0) {
        // recurse until leaves found (-1) or to the level specified
        if (opts.depth == -1 || (level + 1 < opts.depth)) {
            var downLineRow = Ext.DomHelper.append(tbodyObj,"<tr class='"+showStr+"' />");
            var downLineCell = Ext.DomHelper.append(downLineRow,"<td/>");
                        downLineCell.setAttribute('colspan',childNodes.length * 2);
            // draw the connecting line from the parent node to the horizontal line
            downLine = Ext.get(Ext.DomHelper.append(downLineCell,"<div></div>")).addClass("line down");

                        // Draw the horizontal lines
            var linesRow = Ext.DomHelper.append(tbodyObj,"<tr class='"+showStr+"' />");
            Ext.each(childNodes,function (item,index) {
                var left = Ext.get(Ext.DomHelper.append(linesRow,"<td>&nbsp;</td>")).addClass("line left top");
                var right = Ext.get(Ext.DomHelper.append(linesRow,"<td>&nbsp;</td>")).addClass("line right top");
            });

            // horizontal line shouldn't extend beyond the first and last child branches
            Ext.select("td:first",false,linesRow).removeClass("top");
            Ext.select("td:last",false,linesRow).removeClass("top");


            var childNodesRow = Ext.DomHelper.append(tbodyObj,"<tr class='"+showStr+"' />");
            Ext.each(childNodes,function (item,index) {
                var td = Ext.DomHelper.append(childNodesRow,"<td class='node-container'/>");
                                td.setAttribute('colspan',2);
                // recurse through children lists and items
                                sitools.user.modules.ExtJSOrgChart.buildNode(item, td, level + 1, opts);
            });

        }
    }

}
sitools.user.modules.ExtJSOrgChart.prepareTree= function(options){
	var appendTo = Ext.get(options.chartElement);
	var container = Ext.get(Ext.DomHelper.append(appendTo,'<div align="center" class="ExtJSOrgChart"></div>'));
	sitools.user.modules.ExtJSOrgChart.buildNode(options.rootObject,container,0,options);
}
Ext.QuickTips.init();
