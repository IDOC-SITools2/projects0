/***************************************
* Copyright 2010-2013 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
* 
* This file is part of SITools2.
* 
* SITools2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* SITools2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
***************************************/
/*global Ext, sitools, projectGlobal, commonTreeUtils, showResponse, document, i18n, loadUrl, SITOOLS_DEFAULT_PROJECT_IMAGE_URL, SitoolsDesk*/
/*
 * @include "../../env.js" 
 */
Ext.namespace('sitools.user.modules');

/**
 * Dataset Explorer Module.
 * Displays each dataset of the Project.
 * @class sitools.user.modules.datasetExplorerDataView
 * @extends Ext.tree.TreePanel
 */
sitools.user.modules.dataSetExplorerDataviewOchart = Ext.extend(Ext.Panel, {
    layout : "border",
        
    initComponent : function () {
	    /**
	     * INDEX JPB var projectId = Ext.util.Cookies.get('projectId'); if
	     * (Ext.isEmpty(projectId)){ Ext.Msg.alert(i18n.get ('warning.noProject'));
	     * return; }
	     */

         //me.chartConfig = me.chartConfig || {};
    	 //console.log(me.store);
    	var myDataView = new Ext.DataView({
    		id : "chart",
            region : 'center',
            singleSelect : true,
            autoScroll : true,
	    });
    	this.items = [myDataView];
    	var projectAttachment = projectGlobal.sitoolsAttachementForUsers;
    	//alert(projectAttachment);
    	Ext.Ajax.request( {
    		url : projectAttachment + '/graph?media=json',
			//url: 'http://localhost:8182/sitools/client-user/js/modules/dataSetExplorerDataView/ochart/response_sitemap.json',
    		method : "GET",
    		//root : 'graph.nodeList',
		success : function(response, opts) {
			function getChild(n,node) {
   				//nodeList.push(node);
                                //rootNodeList.push(node);
				if(!n.leaf){
					for(var i=0;i<n.children.length;i++){
						var child=new sitools.user.modules.ExtJSOrgChart.createNode(n.children[i]);
						node.addChild(child);
						getChild(n.children[i],child);
					}	
				}
			}
			var rootNodeList=new Array();
			//var nodeList = new Array();
			var data= Ext.decode(response.responseText);
			for(var n=0;n<data.graph.nodeList.length;n++){
				var rootNode = new sitools.user.modules.ExtJSOrgChart.createNode(data.graph.nodeList[n]);
                                //nodeList.push(rootNode);
                                rootNodeList.push(rootNode);
				getChild(data.graph.nodeList[n],rootNode);
			}
			for(var i=0;i<rootNodeList.length;i++){
				sitools.user.modules.ExtJSOrgChart.prepareTree({
					chartElement: 'chart',
					rootObject: rootNodeList[i],
					depth: -1
					});
			}
		} , 
		failure : function(response, opts) {          
				alert("Error while loading data : "+response.responseText);                  
		}
	});	
        
        sitools.user.modules.dataSetExplorerDataviewOchart.superclass.initComponent.call(this);
        
    },
    afterRender : function () {
    	sitools.user.modules.dataSetExplorerDataviewOchart.superclass.afterRender.apply(this, arguments);
       	console.log(this);
    },
    /**
     * method called when trying to save preference
     * @returns
     */
    _getSettings : function () {
		return {
            preferencesPath : "/modules", 
            preferencesFileName : this.id
        };

    }
    
});

Ext.reg('sitools.user.modules.dataSetExplorerDataviewOchart', sitools.user.modules.dataSetExplorerDataviewOchart);
