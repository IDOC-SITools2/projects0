Ext.namespace('sitools.user.modules');

Ext.define( 'sitools.user.modules.DatasetExplorerD3', {
    extend: 'sitools.user.core.Module',


    init : function (moduleModel) {
        var view = Ext.create('sitools.user.view.modules.datasetExplorerD3.DatasetExplorerD3');
        this.setViewCmp(view);

        this.show(this.getViewCmp());

        this.callParent(arguments);
    },

    /**
     * method called when trying to save preference
     * 
     * @returns
     */
    _getSettings : function () {
        return {
            preferencesPath : "/modules",
            preferencesFileName : this.id
        };

    }

});