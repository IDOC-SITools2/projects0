/***************************************
* Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
* 
* This file is part of SITools2.
* 
* SITools2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* SITools2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
***************************************/
/*global Ext, sitools, ID, i18n, document, showResponse, alertFailure, LOCALE, ImageChooser, 
 showHelp*/
Ext.namespace('sitools.component.projects');

Ext.define('sitools.admin.graphs.GraphsNodeWin', {
    extend : 'Ext.Window',
    width : 350,
    modal : true,
    closable : false,

    initComponent : function () {
        projectId = this.projectId;
        this.title = i18n.get('label.nodeDescription');
        /* paramétres du formulaire */
        this.itemsForm = [{
            fieldLabel : i18n.get('label.name'),
            name : 'name',
            anchor : '100%',
            allowBlank : false
        }, {
            fieldLabel : i18n.get('label.description'),
            name : 'description',
            anchor : '100%'
        }, {
            xtype : 'sitoolsSelectImage',
            name : 'image',
            fieldLabel : i18n.get('label.image'),
            anchor : '100%',
            growMax : 400
        },{
            xtype: 'button',
            id: 'linktodataset',
            text: '<span style="">Link to a dataset</span>',
            anchor: '65%',
            margin: '0 5% 0 0',
            handler: function() {
                up = Ext.create("sitools.admin.graphs.GraphsDatasetWin", {
                    graphTree : true,
                    url : loadUrl.get('APP_URL') + '/projects/' + projectId + '?media=json',
                    mode : 'link',
                    form : form
                });
                up.show(this);

            }
        }, {
            xtype: 'button',
            id: 'unlinkbutton',
            text: '<span style="">Unlink</span>',
            disabled:true,
            anchor: '35%',
            margin: '0 0 0 5%',
            handler: function() {
                Ext.getCmp('unlinkbutton').setDisabled(true);
                form.findField('nbRecord').setValue('');
                form.findField('datasetURL').setValue('');
                Ext.getCmp('datatablecheckbox').setValue(false);
                Ext.getCmp('queryformcheckbox').setValue(false);
                Ext.getCmp('datatablecheckbox').setDisabled(true);
                Ext.getCmp('queryformcheckbox').setDisabled(true);
            }
        },{
            name : 'nbRecord',
            fieldLabel : 'Number of records',
            readOnly: true,
            cls: 'x-item-disabled',
            anchor : '100%'
        },{
            name : 'datasetURL',
            fieldLabel : 'Dataset URL',
            readOnly: true,
            cls: 'x-item-disabled',
            anchor : '100%'
        }, {
            xtype: 'checkboxfield',
            boxLabel: 'Data table',
            name: 'datatable',
            width: '40%',
            margin: '0 10% 0 10%',
            disabled: true,
            id: 'datatablecheckbox'
        }, {
            xtype: 'checkboxfield',
            boxLabel: 'Query form',
            name: 'queryform',
            width: '40%',
            margin: '0 10% 0 10%',
            disabled:true,
            id: 'queryformcheckbox'
        } ];
        this.bbar = {
            xtype : 'toolbar',
            defaults : {
                scope : this
            },
            items : [ '->', {
                text : i18n.get('label.ok'),
                handler : this._onOK
            }, {
                text : i18n.get('label.cancel'),
                handler : this._onCancel
            } ]
        };

        this.formPanel = Ext.create('Ext.form.Panel', {
            labelWidth : 100,
            border : false,
            bodyBorder : false,
            padding : '5 5 5 5',
            defaultType : 'textfield',
            items : this.itemsForm

        });

        this.items = [ this.formPanel ];
        this.callParent(arguments);
    },

    afterRender : function () {
        this.callParent(arguments);

        if (this.mode == 'edit') {
            var node = this.node;
            form = this.formPanel.getForm();
            var rec = {};
            if (node.get('text').search("clickDatasetIcone") != -1){
                rec.name = node.get('text').split('<br/>')[0];
                //alert(node.get('text'));
                rec.datasetURL = node.get('text').split('clickDatasetIcone(\'')[1].split('\', \'')[0];
                rec.nbRecord = node.get('nbRecord');
                if (node.get('text').search("Display data") != -1){
                    Ext.getCmp('datatablecheckbox').setValue(true);
                }
                if (node.get('text').search("Query form") != -1){
                    Ext.getCmp('queryformcheckbox').setValue(true);
                }
            } else {
                rec.name = node.get('text');
            }
            rec.image = node.get('image').url;
            rec.description = node.get('description');
            form.setValues(rec);
        } else {
            form = this.formPanel.getForm();
        }
        if (!Ext.isEmpty(form.findField('datasetURL').getValue())){
            Ext.getCmp('linktodataset').setText('<span style="">Link to another dataset</span>');
            Ext.getCmp('unlinkbutton').setDisabled(false);
            Ext.getCmp('datatablecheckbox').setDisabled(false);
            Ext.getCmp('queryformcheckbox').setDisabled(false);

        }
    },

    _onOK : function () {
        var form = this.formPanel.getForm();
        //alert(form.findField('datatable').checked);
        if (!form.isValid()) {
            return;
        }

        var values = form.getValues();

        if (!Ext.isEmpty(values.datasetURL) && !form.findField('datatable').checked && !form.findField('queryform').checked) {
            Ext.Msg.alert("Form error", "Please choose a method to show data.");
            return false;
        }
        var image = {};
        console.log(values);
        if (!Ext.isEmpty(values.image)) {
            image.url = values.image;
            image.type = "Image";
            image.mediaType = "Image";
        }
        if (this.mode == 'edit') {
            if (!Ext.isEmpty(values.datasetURL)) {
                var namee = values.name;
                if (form.findField('datatable').checked || form.findField('queryform').checked) {
                    namee += '<br/>';
                }
                if (form.findField('datatable').checked) {
                    namee += '<a href="#" class="overDatasetService" onClick="sitools.user.utils.DatasetUtils.clickDatasetIcone(\''+values.datasetURL
                    +'\', \'data\');return false;"><img class="datasetochart_icon" src="/sitools/common/res/images/icons/32x32/tree_datasets_32.png" data-qtip="Display data"></a>';
                }
                if (form.findField('queryform').checked) {
                    namee += '<a href="#" class="overDatasetService" onClick="sitools.user.utils.DatasetUtils.clickDatasetIcone(\''+values.datasetURL
                    +'\', \'forms\');return false;"><img class="datasetochart_icon" src="/sitools/common/res/images/icons/32x32/openSearch_32.png" data-qtip="Query form"></a>';
                }
                this.node.set('text', namee);
            } else {
                this.node.set('text', values.name);
            }
            if (!Ext.isEmpty(values.nbRecord)) {
                this.node.set('nbRecord', values.nbRecord);
            } else this.node.set('nbRecord', '0');
            this.node.set('description', values.description);
            this.node.set('image', image);
            console.log(this.node);
            var saveButton = this.graphTree.graphsCrud.down('button#saveGraphBtnId');
            saveButton.addCls('not-save-textfield');
        } else {
            var newNode = Ext.create('sitools.admin.graphs.GraphNodeModel', {
                image : image,
                description : values.description,
                type : "node",
                children : []
            });
            //console.log(newNode);
            if (!Ext.isEmpty(values.nbRecord)) {
                newNode.set('nbRecord', values.nbRecord);
            } else newNode.set('nbRecord', '0');
            if (!Ext.isEmpty(values.datasetURL)) {
                var namee = values.name;
                if (form.findField('datatable').checked || form.findField('queryform').checked) {
                    namee += '<br/>';
                }
                if (form.findField('datatable').checked) {
                    namee += '<a href="#" class="overDatasetService" onClick="sitools.user.utils.DatasetUtils.clickDatasetIcone(\''+values.datasetURL
                    +'\', \'data\');return false;"><img class="datasetochart_icon" src="/sitools/common/res/images/icons/32x32/tree_datasets_32.png" data-qtip="Display data"></a>';
                }
                if (form.findField('queryform').checked) {
                    namee += '<a href="#" class="overDatasetService" onClick="sitools.user.utils.DatasetUtils.clickDatasetIcone(\''+values.datasetURL
                    +'\', \'forms\');return false;"><img class="datasetochart_icon" src="/sitools/common/res/images/icons/32x32/openSearch_32.png" data-qtip="Query form"></a>';
                }
                newNode.set('text', namee);
            } else {
                newNode.set('text', values.name);
            }

            if (!this.node.isExpanded()) {
                this.node.expand();
            }
            this.node.appendChild(newNode);
        }

        //var saveButton = this.graphTree.graphsCrud.down('button#saveGraphBtnId');
        //saveButton.addCls('not-save-textfield');
        //console.log(saveButton);
        this.close();
    },

    _onCancel : function () {
        this.destroy();
    },

    _onUpload : function () {
        Ext.msg.alert("Information", "TODO");
    }

});
