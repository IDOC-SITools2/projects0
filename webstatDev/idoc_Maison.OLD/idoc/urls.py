from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
#    url(r'^stat/', include('webstat.urls', namespace="webstat")),
    url(r'^', include('webstat.urls', namespace="webstat")),
)
