function checkIfPeriod() {
    document.getElementById("dateWebstat").style.display = 'block';
    document.getElementById("id_period_date").checked = true;
    document.getElementById("id_all_date").checked = false;
}

function checkIfAll() {
    document.getElementById("dateWebstat").style.display = 'none';
    document.getElementById("id_start_date").value = "";
    document.getElementById("id_end_date").value = "";
    document.getElementById("id_period_date").checked = false;
    document.getElementById("id_all_date").checked = true;
}

function checkIfDownStat(){
    if(document.getElementById("id_is_down_stat").checked){
        document.getElementById("id_is_down_stat_by_user").checked = true
        document.getElementById("id_is_down_stat_by_ip").checked = true
        document.getElementById("id_is_down_stat_by_country").checked = true
        document.getElementById("id_is_down_stat_by_months").checked = true
        document.getElementById("id_is_down_stat_by_project").checked = true
    }
    if(!document.getElementById("id_is_down_stat").checked){
        document.getElementById("id_is_down_stat_by_user").checked = false
        document.getElementById("id_is_down_stat_by_ip").checked = false
        document.getElementById("id_is_down_stat_by_country").checked = false
        document.getElementById("id_is_down_stat_by_months").checked = false
        document.getElementById("id_is_down_stat_by_project").checked = false
    }
}

function checkIfAccessStat(){
    if(document.getElementById("id_is_access_stat").checked){
        document.getElementById("id_is_access_access_by_ip").checked = true
        document.getElementById("id_is_access_vo_by_ip").checked = true
        document.getElementById("id_is_access_access_by_months").checked = true
        document.getElementById("id_is_access_vo_by_months").checked = true
        document.getElementById("id_is_access_access_by_country").checked = true
        document.getElementById("id_is_access_vo_by_country").checked = true
    }
    if(!document.getElementById("id_is_access_stat").checked){
        document.getElementById("id_is_access_access_by_ip").checked = false
        document.getElementById("id_is_access_vo_by_ip").checked = false
        document.getElementById("id_is_access_access_by_months").checked = false
        document.getElementById("id_is_access_vo_by_months").checked = false
        document.getElementById("id_is_access_access_by_country").checked = false
        document.getElementById("id_is_access_vo_by_country").checked = false
    }
}

function onChangeApp(){
    if(document.getElementById("id_application").value != "HESIOD"){
        document.getElementById("id_is_down_stat_by_project").checked = false
        document.getElementById("id_is_down_stat_by_project").disabled = true
        document.getElementById("id_is_down_stat_by_user").checked = false
        document.getElementById("id_is_down_stat_by_user").disabled = true
    }else{
        document.getElementById("id_is_down_stat_by_project").disabled = false
        document.getElementById("id_is_down_stat_by_user").disabled = false
    }
}
function onLoad(){
    document.getElementById("id_is_access_vo_by_ip").disabled = true
    document.getElementById("id_is_access_vo_by_months").disabled = true
    document.getElementById("id_is_access_vo_by_country").disabled = true
}