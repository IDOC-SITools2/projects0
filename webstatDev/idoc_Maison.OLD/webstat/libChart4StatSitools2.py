#!/usr/bin/env python
# -*-coding:utf-8-*-

import sys
try:
    import random
except BaseException:
    sys.exit("Import failed in module libStatSitools :\n\trandom module is required")
try:
    import cairo
except BaseException:
    sys.exit("Import failed in module libStatSitools :\n\tcairo module is required")
try:
    import pycha.pie as pie
except BaseException:
    sys.exit("Import failed in module libStatSitools :\n\tpycha module is required")
try:
    from datetime import datetime
except BaseException:
    sys.exit("Import failed in module libStatSitools :\n\tdatetime module is required")
try:
    import pygal
except BaseException:
    sys.exit("Import failed in module libStatSitools :\n\tpygal module is required")
try:
    from dateutil.relativedelta import relativedelta
except:
    sys.exit("Import failed in module libStatSitools :\n\tdateutil.relativedelta module is required")

#PATH FOR DEV
home_base = "/home/marc/MyDev/sitools-idoc/webstatDev/idoc_Maison"
#PATH FOR PROD
#home_base = "/var/www/django/mon_projet/"

url_png = "/static/chart/"

time_format_for_by_month = "%Y %m"

def create_dataset_from_data(dico_data):
    graph_dataset = [(data, [[0, dico_data[data]]]) for data in dico_data]
    return graph_dataset


def add_date():
    now = datetime.now()
    tmp = str(now).split(" ")
    date_to_add = tmp[0].split("-")[0]+tmp[0].split("-")[1]+tmp[0].split("-")[2]+tmp[1].split(".")[0].split(":")[0]+tmp[1].\
        split(".")[0].split(":")[1]+tmp[1].split(".")[0].split(":")[2]+tmp[1].split(".")[1]
    return date_to_add


def svg_chart(dico_data, title_chart):

    pie_chart_pygal = pygal.Pie()
    pie_chart_pygal.title = title_chart
    date = add_date()

    url_output_svg = url_png+title_chart+date+".svg"
    print "**********************************************"
    print "*************  Nb entrée pour", title_chart, " : ", len(dico_data)
    print "**********************************************"
    dico_data_tmp = dico_data.copy()
    if len(dico_data) > 24:
        dico_data2 = {}
        i = 0
        for key in dico_data_tmp:
            if i > 24:
                dico_data2[key] = dico_data_tmp[key]
                del dico_data[key]
            i += 1


    for data in dico_data:
            pie_chart_pygal.add(data, dico_data[data])

    if len(dico_data_tmp) > 24:
        for datas in dico_data2:
            pie_chart_pygal.add(datas, dico_data2[datas], secondary=True)

    output_svg = home_base+url_output_svg
    try:
        pie_chart_pygal.render_to_file(output_svg)
    except BaseException, e:
        print 'In svg Exception : ', str(e)

    url_chart_png = png_chart(dico_data, title_chart)
    return [url_output_svg, url_chart_png]


def png_chart(dico_data, title_chart):

    pie_chart_pygal_png = pygal.Pie()
    pie_chart_pygal_png.title = title_chart
    
    date = add_date()
    url_output_png = url_png+title_chart+date+".png"
    for data in dico_data:
        pie_chart_pygal_png.add(data, dico_data[data])
    output_png = home_base+url_output_png
    try:
        pie_chart_pygal_png.render_to_png(output_png)
    except BaseException, e:
        print 'In PNG Exception : ', str(e)
    return url_output_png


def svg_bar_chart(dico_data, start_date, end_date, title_chart):

    dates_tmp_not_sorted = []
    dates_tmp_str_sorted = []
    dates_sorted = []
    dico_data2 = {}
    for date in dico_data.keys():
        dates_tmp_not_sorted.append(str(date[1])+"/"+str(date[0]))

    for dd in sorted(dates_tmp_not_sorted, key=lambda d: map(int, d.split('/'))):
        dates_tmp_str_sorted.append(dd.split('/')[1]+"/"+dd.split('/')[0][2:])

#    date_min = datetime.strptime(dates_tmp_str_sorted[0], "%m/%y")
#    date_max = datetime.strptime(dates_tmp_str_sorted[len(dates_tmp_str_sorted)-1], "%m/%y")

    for i in range(0, int(diff_month(start_date, end_date)+1)):
        t = datetime.combine(start_date, datetime.min.time())+relativedelta(months=i)
        dates_sorted.append(t)

    for x in dates_tmp_str_sorted:
        date_test = datetime.strptime(str(x), "%m/%y")
        dico_data2[date_test] = dico_data[(date_test.month, date_test.year)]

    list_tmp = []
    list_tmp2 = []
    list_tmp.append("")

    bar_chart = pygal.DateY(x_label_rotation=90, human_readable=True, y_scale=1073741824)
    bar_chart.title = title_chart
    bar_chart.x_label_format = "%m/%Y"
    bar_chart.x_labels = dates_sorted
    print "************* dates_sorted : ", dates_sorted
    try:
        for data in dates_sorted:
            if data not in dico_data2.keys():
                dico_data2[data] = 0
            list_tmp2.append((data, dico_data2[data]))
        list_tmp.append(list_tmp2)

        bar_chart.add(list_tmp[0],  (list_tmp[1][i] for i in range(0, len(list_tmp[1]))))
        bar_chart.show_legend = False
        date_svg = add_date()
        url_output_bar_chart_svg = url_png+"Bar_by_Month_"+date_svg+".svg"
        bar_chart.render_to_file(home_base+url_output_bar_chart_svg)
        url_output_bar_chart_png = url_bar_chart_png(list_tmp, dates_sorted, title_chart)
    except BaseException, e:
        print str(e)
    return [url_output_bar_chart_svg, url_output_bar_chart_png]


def url_bar_chart_png(list_data, dates, title):
    bar_chart = pygal.DateY(x_label_rotation=90)
    bar_chart.title = title
    bar_chart.x_label_format = "%m/%Y"
    bar_chart.x_labels = dates
    bar_chart.add(list_data[0],  (list_data[1][i] for i in range(0, len(list_data[1]))))
    # A METTRE POUR CREER LE PNG !
    bar_chart.print_values = False
    bar_chart.show_legend = False
    url_output_png = url_png+"Bar_by_Month_"+add_date()+".png"
    bar_chart.render_to_png(home_base+url_output_png)
    return url_output_png


def diff_month(d2, d1):
    return (d1.year - d2.year)*12 + d1.month - d2.month
