#-*-coding:utf-8-*-

from django.shortcuts import render, get_object_or_404
from webstat.forms import webstatForm
import models
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from webstat.libStatSitools import *

'''
def home(request):
    text = "Welcome on SiTools2 Webstat Interface"
    return render(request, 'home.html', locals())
'''

def statview(request):

    if request.method == 'POST':
        form = webstatForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                form_id = models.webstatModel.objects.filter().order_by('-id')[0].id
                request.session['form_id'] = form_id
            except BaseException, e:
                print "Save FAILED : ", str(e)
                return render(request, '500.html', locals())
            return HttpResponseRedirect(reverse('webstat:results'))
    else:
        form = webstatForm()
    return render(request, 'formWebStat.html', locals())


def result_view(request):

    form_id = request.session.get('form_id')
    form = get_object_or_404(models.webstatModel, pk=form_id)

    if form.start_date is None:
        start_tmp = "2012-03-01 00:00:00"
        form.start_date = datetime(int(start_tmp[0:4]), int(start_tmp[5:7]), int(start_tmp[8:10]),
                                   int(start_tmp[11:13]), int(start_tmp[14:16])).date()
    if form.end_date is None:
        form.end_date = datetime.now().date()

    form_dico = instance_dict(form)
    data = stat_log(form.application, form_dico)
    return render(request, 'webStatResults.html', locals())


def instance_dict(instance, key_format=None):
    "Returns a dictionary containing field names and values for the given instance"
    from django.db.models.fields.related import ForeignKey
    if key_format:
        assert '%s' in key_format, 'key_format must contain a %s'
    key = lambda key: key_format and key_format % key or key

    d = {}
    for field in instance._meta.fields:
        attr = field.name
        value = getattr(instance, attr)
        if value is not None and isinstance(field, ForeignKey):
            value = value._get_pk_val()
        d[key(attr)] = value
    for field in instance._meta.many_to_many:
        d[key(field.name)] = [obj._get_pk_val() for obj in getattr(instance, field.attname).all()]
    return d
