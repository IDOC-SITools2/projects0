#!/usr/bin/env python
#-*-coding:utf-8-*-

__version__ = "0.1"
__license__ = "GPL"
__author__ = "Marc NICOLAS"
__credit__ = "Marc NICOLAS"
__maintainer__ = "Marc NICOLAS"
__email__ = "marc.nicolas@ias.u-psud.fr"
from django.db import models


class webstatModel(models.Model):
    HESIOD = 'HESIOD'
    appChoices = (
        ('HESIOD', 'HESIOD'),
        ('SDO', 'SDO'),
        ('SZCLUSTER', 'SZCLUSTER'),
        ('COROT', 'COROT'),
    )

    application = models.CharField(max_length=55, choices=appChoices, default=HESIOD)

    all_date = models.BooleanField(default=False)
    period_date = models.BooleanField(default=False)

    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    #is_access_stat = models.BooleanField(default=False)

    is_access_access_by_ip = models.BooleanField(default=False)
    is_access_access_by_months = models.BooleanField(default=False)
    is_access_access_by_country = models.BooleanField(default=False)

#    is_access_vo_by_ip = models.BooleanField(default=False)
#    is_access_vo_by_months = models.BooleanField(default=False)
#    is_access_vo_by_country = models.BooleanField(default=False)

    #is_down_stat = models.BooleanField(default=False)

    is_down_stat_by_user = models.BooleanField(default=False)
    is_down_stat_by_ip = models.BooleanField(default=False)
    is_down_stat_by_country = models.BooleanField(default=False)
    is_down_stat_by_months = models.BooleanField(default=False)
    is_down_stat_by_project = models.BooleanField(default=False)
    is_just_down_stat = models.BooleanField(default=True)

    #nb_element = models.IntegerField()

