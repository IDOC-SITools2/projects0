from django import template
register = template.Library()

#!/usr/bin/env python
#-*-coding:utf-8-*-

__version__ = "0.1"
__license__ = "GPL"
__author__ = "Marc NICOLAS"
__credit__ = "Marc NICOLAS"
__maintainer__ = "Marc NICOLAS"
__email__ = "marc.nicolas@ias.u-psud.fr"

@register.filter()
def per_cent_filters(value):
    print "VALUE : ", value
    return value * 100

