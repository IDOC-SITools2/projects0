from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
#    url(r'^home$', 'webstat.views.statview', name='accueil'),
    url(r'^$', 'webstat.views.statview', name='accueil'),
    url(r'^results$', 'webstat.views.result_view', name='results')
)+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
