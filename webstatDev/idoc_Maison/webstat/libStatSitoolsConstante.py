#!/usr/bin/env python
#-*-coding:utf-8-*-

__version__ = "0.1"
__license__ = "GPL"
__author__ = "Marc NICOLAS"
__credit__ = "Marc NICOLAS"
__maintainer__ = "Marc NICOLAS"
__email__ = "marc.nicolas@ias.u-psud.fr"


def defCst(app):

    dico_cst = {}

    dico_cst["time_format"] = "%Y-%m-%d %H:%M:%S"
    dico_cst["time_format_for_by_month"] = "%Y-%m-%d"

    dico_cst["pattern_Fits"] = ".fits"
    dico_cst["pattern_Tar"] = ".tar"
    dico_cst["pattern_download"] = [".fits", ".tar"]
    dico_cst["pattern_http_method"] = "GET"
    dico_cst["pattern_code_retour"] = "200"
    dico_cst["patternBot"] = "bot.html"
    dico_cst["patternYahoo"] = "http://help.yahoo.com"
    dico_cst["patternBaidu"] = ["baidu.com","Baiduspider", "+http://www.baidu.com/search", "spider"]

    dico_cst["CutFits"] = ["storageFitsCut", "cut", "Cut"]
    dico_cst["sia"] = ["sia", "SIA"]

    dico_cst["dico_max_per_cent"] = {"is_access_access_by_country": 0.005, "is_down_stat_by_project": 0.005,
                                     "is_down_stat_by_country": 0.005, "is_down_stat_by_ip": 0.005,
                                     "is_access_access_by_ip": 0.005, "is_access_access_by_country": 0.005}

    # SZ CLUSTER INSTANCE
    if app == "SZCLUSTER":
        #PATH FOR  DEV
        #dico_cst["listPathLogs"] = {"/home/marc/log/logSZ/"}
        #PATH FOR PROD
        dico_cst["listPathLogs"] = {"/usr/local/Sitools2_Logs/Sitools2_SZ_Cluster_DB/logs"}
        dico_cst["patternAccessToSeek"] = "/sitools/client-user/SZCLUSTER_DATABASE/project-index.html"
        dico_cst["patternVoConeSearchToSeek"] = "/global_dataset/plugin/conesearch"
        dico_cst["patternVoSiaToSeek"] = ""
        dico_cst["listIpToExclude"] = ["129.175.64.124", "129.175.65.119", "129.175.64.65", "129.175.67.137",
                                       "129.175.64.230", "172.18.60.17", "129.175.65.131", "129.175.65.82","129.175.65.115",
                                       "129.175.66.42", "127.0.0.1", "129.175.66.43", "129.175.66.248", "129.175.68.20",
                                       "129.175.64.25","220.181.108.118","123.125.71.31","123.125.71.31"]
        dico_cst["dicoProject"] ={}

        # HESIOD INSTANCE
    elif app == "HESIOD":
        # PATH FOR DEV
        #dico_cst["listPathLogs"] = {"/home/marc/log/logH", "/home/marc/log/logH_0.9.1", "/home/marc/log/logH_1.0","/home/marc/log/logH_2.0"}
        #PATH FOR PROD
        dico_cst["listPathLogs"]= {"/usr/local/Sitools2_Logs/Sitools2_Herschel/logs","/usr/local/Sitools2_Logs/old_Sitools2_instances/Sitools2_Herschel_old/Sitools2_Herschel_PROD_V1.0/workspace/fr.cnes.sitools.core/logs","/usr/local/Sitools2_Logs/old_Sitools2_instances/Sitools2_Herschel_old/Sitools2_Herschel_0.9.1_old/workspace/fr.cnes.sitools.core/logs"}
        dico_cst["patternAccessToSeek"] = "/sitools/client-user/"
        dico_cst["patternVoConeSearchToSeek"] = ""
        dico_cst["patternVoSiaToSeek"] = "sia"
        dico_cst["listIpToExclude"] = ["129.175.64.124", "129.175.65.119", "129.175.64.65", "129.175.67.137",
                                       "129.175.64.230", "172.18.60.17", "129.175.65.131", "129.175.65.82",
                                       "129.175.66.42",  "129.175.66.43", "129.175.66.248", "129.175.68.20",
                                       "129.175.64.25", "220.181.108.118"]
        dico_cst["dicoProject"] = {"sag-4": "SAG-4", "herschel": "Herschel", "cluster-low-z": "Cluster-Low-Z",
                                   "sag-3": "SAG-3", "ddt_mustdo_4": "DDT_MustDo_4", "lens_malhotra": "Lens_Malhotra",
                                   "ot1_atielens": "OT1_atielens", "ot2_ehabart": "OT2_ehabart", "ot1_lho": "OT1_lho",
                                   "ot1_mmiville": "OT1_mmiville", "sag-1": "SAG-1", "h-atlas":"H-ATLAS",
                                   "ot2_hdole": "Planck-High-z", "ot1_lmontier": "Planck-High-z",
                                   "ddt_mustdo_5": "Planck-High-z", "planck%20high-z": "Planck-High-z",
                                   "herschel%20private": "Herschel-Private", "herschel-private": "Herschel-Private",
                                   "others": "Others", "SAG-4_IAS": "SAG-4"}

        # SDO INSTANCE
    elif app == "SDO":
        dico_cst["listPathLogs"] = {"/usr/local/Sitools2_Logs/Sitools2_SDO/logs"}
        dico_cst["patternAccessToSeek"] = "/sitools/client-user/SZCLUSTER_DATABASE/project-index.html"
        dico_cst["patternVoConeSearchToSeek"] = "/global_dataset/plugin/conesearch"
        dico_cst["patternVoSiaToSeek"] = ""
        dico_cst["listIpToExclude"] = ["129.175.64.124", "129.175.65.119", "129.175.64.65", "129.175.67.137",
                                       "129.175.64.230", "172.18.60.17", "129.175.65.131", "129.175.65.82",
                                       "129.175.66.42", "127.0.0.1", "129.175.66.43", "129.175.66.248", "129.175.68.20",
                                       "129.175.64.25"]
        dico_cst["dicoProject"] ={}

        # COROT INSTANCE
    elif app == "COROT":
        dico_cst["listPathLogs"] = {"/usr/local/Sitools2_Logs/Sitools2_Corot/logs"}
        dico_cst["patternAccessToSeek"] = "/sitools/client-user/SZCLUSTER_DATABASE/project-index.html"
        dico_cst["patternVoConeSearchToSeek"] = "/global_dataset/plugin/conesearch"
        dico_cst["patternVoSiaToSeek"] = ""
        dico_cst["listIpToExclude"] = ["129.175.64.124", "129.175.65.119", "129.175.64.65", "129.175.67.137",
                                       "129.175.64.230", "172.18.60.17", "129.175.65.131", "129.175.65.82",
                                       "129.175.66.42", "127.0.0.1", "129.175.66.43", "129.175.66.248", "129.175.68.20",
                                       "129.175.64.25"]
        dico_cst["dicoProject"] ={}

    # PICARD INSTANCE
    elif app == "PICARD":

        dico_cst["listPathLogs"] = {"/home/marc/Projet/LOG_PICARD/logs"}
        dico_cst["patternAccessToSeek"] = "/sitools/client-user/Picard/project-index.html"
        dico_cst["patternVoConeSearchToSeek"] = ""
        dico_cst["patternVoSiaToSeek"] = ""
        dico_cst["listIpToExclude"] = ["129.175.64.124", "129.175.65.119", "129.175.64.65", "129.175.67.137",
                                       "129.175.64.230", "172.18.60.17", "129.175.65.131", "129.175.65.82",
                                       "129.175.66.42", "127.0.0.1", "129.175.66.43", "129.175.66.248", "129.175.68.20",
                                       "129.175.64.25","129.175.64.126","129.175.65.117","129.175.65.105"]
        dico_cst["dicoProject"] ={}
    return dico_cst