#-*-coding:utf-8-*-
from django import forms
from models import webstatModel
from datetimewidget.widgets import DateTimeWidget,DateWidget


class webstatForm(forms.ModelForm):

    class Meta:

        model = webstatModel
        fields = ['application', 'all_date', 'period_date', 'start_date', 'end_date',
                  'is_access_access_by_ip', 'is_access_access_by_months', 'is_access_access_by_country',
                  #'is_access_vo_by_ip', 'is_access_vo_by_months', 'is_access_vo_by_country',
                  'is_down_stat_by_user', 'is_down_stat_by_ip', 'is_down_stat_by_country', 'is_down_stat_by_months',
                  'is_down_stat_by_project', 'is_just_down_stat']
                  #, 'nb_element'
        labels = {
            'application': 'Webapp SITools2 ',
            'start_date': 'Du ',
            'end_date': ' Au ',
            'all_date': 'Tous',
            'period_date': 'Sur une période',
            'is_access_access_by_ip': 'Access by Ip',
            'is_access_access_by_months': 'Access by Month',
            'is_access_access_by_country': 'Access by Country',
            #'is_access_vo_by_ip': 'Vo Access by Ip',
            #'is_access_vo_by_months': 'Vo Access by Month',
            #'is_access_vo_by_country': 'Vo Access by Country',
            'is_down_stat_by_user': 'Down by User',
            'is_down_stat_by_ip': 'Down by Ip',
            'is_down_stat_by_country': 'Down by Country',
            'is_down_stat_by_months': 'Down by Month',
            'is_down_stat_by_project': 'Down by Project',
            'is_just_down_stat': 'Just Down',
        }

	dateTimeOptions = {
                'format': 'dd/mm/yyyy',
                #'startView': '',
                'minView': 2,
                'maxView': 2,
                'autoclose': True,
                'clearBtn': 'true',
        }

        widgets = {
            'all_date': forms.CheckboxInput(attrs={"onClick": 'checkIfAll()'}),
            'period_date': forms.CheckboxInput(attrs={"onClick": 'checkIfPeriod()'}),
#            'start_date': DateWidget(usel10n=True, bootstrap_version=3),
#            'end_date': DateWidget(usel10n=True, bootstrap_version=3),
	    #'is_down_stat': forms.CheckboxInput(attrs={"onClick": 'checkIfDownStat()'}),
            #'is_access_stat': forms.CheckboxInput(attrs={"onClick": 'checkIfAccessStat()'}),
            'application': forms.Select(attrs={"onClick": 'onChangeApp()'}),
        }
