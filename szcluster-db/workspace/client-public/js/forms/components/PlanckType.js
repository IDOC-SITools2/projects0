/***************************************
 * Copyright 2011 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 * 
 * This file is part of SITools2.
 * 
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************/
/*global Ext, sitools*/
//Ext.ns('sitools.component.users.SubSelectionParameters.SingleSelection');
Ext.ns('sitools.common.forms.components')

//sitools.component.users.SubSelectionParameters.SingleSelection.PlanckType = Ext.extend(Ext.Container, {

sitools.common.forms.components.PlanckType = Ext.extend(Ext.Container, {

	initComponent : function () {
		var defaultValue = "", value, items=[];
		items.push(['', '']);
		/*items.push(['20', 'Known clusters (type = 20)']);
		items.push(['10', 'New confirmed (type = 10)']);
		items.push(['1', 'New candidates-classe 1 (type = 1)']);
		items.push(['2', 'New candidates-classe 2 (type = 2)']);
		items.push(['3', 'New candidates-classe 3 (type = 3)']);*/
		items.push(['20', 'Known clusters']);
		items.push(['10', 'New confirmed']);
		items.push(['1', 'New candidates']);


		var store;
		store = new Ext.data.ArrayStore({
			fields : [ 'id', 'value' ],
			data : items
		});

		this.combo = new Ext.form.ComboBox ({
			store : store,
			parameterId : this.parameterId, 
			sParentParam : this.parentParam, 
			valueField : 'id',
			displayField : 'value',
			typeAhead : true,
			mode : 'local',
			triggerAction : 'all',
			selectOnFocus : true,
			allowBlank : true,
			editable : false,
			autoSelect : false,
			flex : 1, 
			height : this.height,
			value : defaultValue,
			tpl : '<tpl for="."><div class="x-combo-list-item comboItem">{value}</div></tpl>', 
			stype : "sitoolsFormItem", 
			/**
			 * The Parent Window.
			 */
			parent : "panelResultForm" + this.formId, 

			/**
			 * The code of the parameter to notify changed event.
			 */
			code : this.code,
			anchor : '90%', 
			listeners : {
				scope : this, 
				'select' : function () {
					var parentPanel = this.ownerCt.ownerCt;
					parentPanel.fireEvent('componentChanged', parentPanel, this);
				}
			}
		});
		
		Ext.apply(this, {
			stype : "sitoolsFormContainer",
			layout : "hbox",
			items : [new Ext.Container({
				border : false,
				html : this.label,
				width : 50
			}), this.combo, new Ext.Container({border : false,
				html : '&nbsp;'}), new Ext.Container({
				border : false,
				html : '&nbsp;',
				width : 5
			})]
		});
		//sitools.component.users.SubSelectionParameters.SingleSelection.PlanckType.superclass.initComponent.apply(this,
		sitools.common.forms.components.PlanckType.superclass.initComponent.apply(this,
				arguments);
	},

	notifyValueSelected : function () {
		this.parent.notifyValueChanged(this.code);
	},

	isValueDefined : function () {
		if (this.combo.getValue() && this.combo.getValue() !== "") {
			return true;
		} else {
			return false;
		}
	},
	getSelectedValue : function () {
		if (this.combo.getValue() && this.combo.getValue() !== "") {
			return this.combo.getValue();
		} else {
			return null;
		}
	},
	setSelectedValue : function (value) {
		this.combo.setValue(value);
	},
	getParameterValue : function () {
		var value = this.getSelectedValue();
		if (Ext.isEmpty(value)) {
			return null;
		}
		
//		alert( this.type + "|" + value);
//		return this.type + "|" + this.code + "|" + value + "|" + probaval;
		//return this.type + "|" + value;
		if (value == "1") {
			return {
			    	type : this.type, 
		    		code : "validation", 
				value : '2'+"|"+'1'+"|"+'3'
			}
		} 
		return {
			type : this.type,
                        code : "validation",
                       	value : value
		}
	},

//  *** Reset function for RESET button ***//
    resetToDefault : function () {
	this.combo.reset();
    }
//  ***************************************//


});
