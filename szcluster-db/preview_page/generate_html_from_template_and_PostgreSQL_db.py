#!/usr/bin/python
# script to make HTML files used by demonstrator for Sitools SZ DB...
# HTML template from Boualam
# Script from Elie
# Psycopg2 code from Alessandro

html_template = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<!--<link rel="stylesheet" type="text/css" href="style.css"/>-->
	<!--Original dark blue: #005fbf-->
	<!--Original light blue: #aad4ff-->
	<style type="text/css">
		body {
		background: #FFFFFF;
		color: #EEE;
		text-align: center;
		font: normal 9pt Verdana;
	    }
	    img {
		border: none;
	    }
	    #normal2, #normal3, #normal4, #normal5 {
		display: none;
	    }
	    #gallery {
		margin:1%% auto;
	    }
	    .album{
	    	width:55%%;
	    	float:left;
	    	margin-left: 5%%
	    }
	    #thumbs {
		margin-right: 10%%;
		width: 5%%;
		height: 60%%;
		float: left;
	    }
	    #thumbs img {
		width: 248%%;
		height: 248%%;
		margin: 2%%;
		box-shadow: 3px 3px 1px #ccc;
	    }
	    #thumbs a:hover {
		margin: 0 0 0 2%%;
	    }
	    .big{
	    	margin-bottom: 5%%;
	    }
	    #bigimages {
	    }
	    #bigimages img {
		width: 63%%;
		height:63%%;
		box-shadow: 3px 3px 1px #ccc;
		float:left;
	    }
	    #arrow {
		margin-top: 27%%;
		width: 10%%;
		float: left;
	    }
	    #arrow img {
		width: 100%%;
	    }
	    #cds{
	    	text-align:center;
	    }
	    #cds img {
        width: 20%%;
        }
        #information {
        	float:left;
			padding:0px;
			width:26%%;
			border:1px solid #000000;
			margin-left: 5%%;
		}#information table{
		    border-spacing: 0;
			width:100%%;
			margin:0px;padding:0px;
		}
		#information tr:nth-child(odd){ background-color:#F0F0F0; } 
		#information tr:nth-child(even){ background-color:#ffffff; }
		#information td{
			vertical-align:middle;
			border:1px solid #000000;
			text-align:right;
			font-size:1.2em;
			font-family:Arial;
			font-weight:normal;
			color:#000000;
			padding: 3%% 3%% 3%% 5%%;
		}
		#information tr:first-child td{
			background-color:#404040;
			text-align:center;
			padding:2%%;
			font-size:2em;
			font-family:Arial;
			font-weight:bold;
			color:#ffffff;
		}
		ul{
			padding: 0;
			margin:0;
		}
		ul li{
			list-style:none;
		}
	</style> 

	<script type="text/javascript">

		var currentGlobal = 1;

		function changeImage(current,total) {
			//var imagesNumber = 6;
			for (i=1; i<=total; i++) {	
				if (i == current) {
					document.getElementById("normal" + current).style.display = "block";
				} else {
					document.getElementById("normal" + i).style.display = "none";
				}
			}
			currentGlobal = current;
		}

		function next() {
			if (currentGlobal==5){currentGlobal=0};
		        changeImage(currentGlobal+1,5);
		}

		function previous() {
			if (currentGlobal==1){currentGlobal=6};
		        changeImage(currentGlobal-1,5);
		}

		window.addEventListener('keyup', function (e) {
		    if (e.keyCode === 37) {previous()}
		    if (e.keyCode === 39) {next()}
		}, false);

	</script>

</head>

<body>
  
<div id="gallery">	

	<div class="album">
	    <div class="big">
		<div id="thumbs">
		    <a href="javascript: changeImage(1,5);"><img src="../%(DATA_DIR)s/%(SZ_FILE)s" alt="" /></a>
		    <a href="javascript: changeImage(2,5);"><img src="../%(DATA_DIR)s/%(SZ_SDSS_FILE)s" alt="" /></a>
		    <a href="javascript: changeImage(3,5);"><img src="../%(DATA_DIR)s/%(SZ_W1_FILE)s" alt="" /></a>
		    <a href="javascript: changeImage(4,5);"><img src="../%(DATA_DIR)s/%(SZ_FOF_FILE)s" alt="" /></a>
		    <a href="javascript: changeImage(5,5);"><img src="../%(DATA_DIR)s/%(SZ_ROSAT_FILE)s" alt="" /></a>
		</div>

		<div id="arrow"><a href="javascript: previous();"><img src="../%(DATA_DIR)s/prev.png" alt="" /></a></div>

		<div id="bigimages">
		  <div id="normal1"><a href="javascript: next();"><img src="../%(DATA_DIR)s/%(SZ_FILE)s" alt=""/></a></div>
		  <div id="normal2"><a href="javascript: next();"><img src="../%(DATA_DIR)s/%(SZ_SDSS_FILE)s" alt=""/></a></div>
		  <div id="normal3"><a href="javascript: next();"><img src="../%(DATA_DIR)s/%(SZ_W1_FILE)s" alt=""/></a></div>	
		  <div id="normal4"><a href="javascript: next();"><img src="../%(DATA_DIR)s/%(SZ_FOF_FILE)s" alt=""/></a></div>	
		  <div id="normal5"><a href="javascript: next();"><img src="../%(DATA_DIR)s/%(SZ_ROSAT_FILE)s" alt=""/></a></div>   
		</div>
		
		<div id="arrow"><a href="javascript: next();"><img src="../%(DATA_DIR)s/next.png" alt="" /></a></div>
		<div style="clear:both;"></div> 
	    </div>
	</div>
	<div id="information">
                <table >
                    <tr><td colspan="2">%(NAME)s</td></tr>
                    <tr>
                    	<td><b>Index</b></td>
                        <td style='text-align:left'>%(INDEX)i</td>
                    </tr>
                    <tr>
                        <td><b>RA</b></td>
                        <td style='text-align:left'>%(RA)s</td>
                    </tr>
                    <tr>
                        <td><b>DEC</b></td>
                        <td style='text-align:left'>%(DEC)s</td>
                    </tr>
                    <tr>
                        <td><b>Redshift</b></td>
                        <td style='text-align:left'>%(REDSHIFT)s</td>
                    </tr>
                    <tr>
                        <td><b>Redshift type</b></td>
                        <td style='text-align:left'>%(REDSHIFT_TYPE)s</td>
                    </tr>
                    <tr>
                        <td><b>Redshift ref.</b></td>
                        <td style='text-align:left'>%(REDSHIFT_REF)s</td>
                        
                    </tr>
                    <tr>
			%(ALT_NAME_BLOCK)s
                    </tr>
                    <tr>
                       <td><b>VO Link</b></td>
                       <td id="cds"  style='text-align:left'><a href="http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=%(RA)s %(DEC)s&submit=SIMBAD+search" target="_blank"><img src="../%(DATA_DIR)s/cds_icon.gif" alt="" /></a></td>                       
                    </tr>
                </table>  
	</div>
	
</div>

</body>
</html>
"""

import psycopg2

## *** Customize here for the server *** ##
dbname = "'<DB_NAME>'"
user = "'postgres'"
host = "'localhost'"
pwd = "''"
dataset = "'<DATASET_NAME>'"
conn = psycopg2.connect("dbname="+dbname+" user="+user+" host="+host+" password="+pwd+"")

cur = conn.cursor()

# To get the number of rows
cur.execute("SELECT COUNT(*) FROM %s;" % dataset)
index_max = int(cur.fetchall()[0][0])

for i in range(0, index_max):
    output_file = './html/index_%d.html' % (i+1)
    
    try:
      cur.execute("select index, name, ra, dec, redshift, redshift_type, redshift_ref, alt_name, psz1_id_ext from "+dataset+" where index="+str(i+1)+"")
    except:
      print 'Data access impossible for object with Index: ', i+1

    rows = cur.fetchall()[0]
    # custumize here if necessary...

    DATA_dir = 'images'
    SZ_file = 'SZ_%d_SZ.png' % i
    SZ_SDSS_file = 'SZ_%d_SZ_SDSS.png' % i
    SZ_W1_file = 'SZ_%d_SZ_W1.png' % i
    SZ_FOF_file = 'SZ_%d_SZ_FoF.png' % i
    SZ_ROSAT_file = 'SZ_%d_SZ_ROSAT.png' % i

    INDEX =           int(rows[0])
    NAME =            rows[1]
    RA =              str(rows[2])
    DEC =             str(rows[3])
    REDSHIFT =        str(rows[4])
    REDSHIFT_TYPE =   rows[5]
    REDSHIFT_REF =    rows[6]
    if str(REDSHIFT_REF).upper() in ['NULL', 'NONE']: REDSHIFT_REF = ''
    
    ALT_NAME =        rows[7]
    ID_EXT =          rows[8] #rows[1]

    try:
      single_alt_names = ALT_NAME.split(';')
    except:
      single_alt_names = ['']
    
    ALT_NAME_BLOCK = """<td><b>Alt. Name</b></td>
          <td style='text-align:left'>
	  <ul>"""
    for names in single_alt_names:
      ALT_NAME_BLOCK += """
      	    <li>%s</li>""" % names.strip()
    ALT_NAME_BLOCK += """
          </ul>
          </td>"""

    html_current = html_template % {
        'DATA_DIR' : DATA_dir,
        'SZ_FILE' : SZ_file,
        'SZ_SDSS_FILE' : SZ_SDSS_file,
        'SZ_W1_FILE' : SZ_W1_file,
        'SZ_FOF_FILE' : SZ_FOF_file, 
        'SZ_ROSAT_FILE' : SZ_ROSAT_file,
        
        'NAME' : NAME,	
        'INDEX' : INDEX,
        'RA' : RA,
        'DEC' : DEC,
        'REDSHIFT' : REDSHIFT,
        'REDSHIFT_TYPE' : REDSHIFT_TYPE,
        'REDSHIFT_REF' : REDSHIFT_REF,
        'ALT_NAME' : ALT_NAME,
        'ID_EXT' : ID_EXT,
	'ALT_NAME_BLOCK' : ALT_NAME_BLOCK
        }

    
    file_out = open(output_file,'w')
    file_out.write(html_current)
    file_out.close()

    
