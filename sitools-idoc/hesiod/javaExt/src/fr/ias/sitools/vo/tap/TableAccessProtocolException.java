/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ias.sitools.vo.tap;

import fr.cnes.sitools.common.exception.SitoolsException;

/**
 * @author marc
 */
public class TableAccessProtocolException extends SitoolsException {

    public TableAccessProtocolException(String message) {
        super(message);
    }
}
