/*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of SITools2.
 *
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.cnes.sitools.resources.order.representations;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.restlet.Context;
import org.restlet.data.ClientInfo;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Reference;
import org.restlet.representation.OutputRepresentation;
import org.restlet.representation.Representation;

import fr.cnes.sitools.common.exception.SitoolsException;
import fr.cnes.sitools.resources.order.utils.ListReferencesAPI;
import fr.cnes.sitools.resources.order.utils.OrderResourceUtils;

/**
 * Representation used to create a Zip Archive from a List of {@link Reference}
 * pointing to some files
 * 
 * 
 * @author m.gond
 */
public class ZipOutputRepresentation extends OutputRepresentation {
  /**
   * the list of {@link Reference}
   */
  private List<Reference> references;
  /** The clientInfo */
  private ClientInfo clientInfo;
  /** The Context */
  private Context context;
  /** The source reference map */
  /**name for file in tar ball hash table */
  private Hashtable Listname;
  private String ipClient;
  private Map<Reference, String> refMap;

  /**
   * Create a new {@link ZipOutputRepresentation}
   * 
   * @param referencesSource
   *          the list of {@link Reference} pointing the file to add to the zip
   *          archive
   * @param clientInfo
   *          the clientInfo of the current user
   * @param context
   *          the Context
   * @param fileName
   *          the complete fileName with extension
   */
  public ZipOutputRepresentation(List<Reference> referencesSource, ClientInfo clientInfo, Context context,
      String fileName) {
    super(MediaType.APPLICATION_ZIP);
    references = referencesSource;
    this.clientInfo = clientInfo;
    this.context = context;
    this.Listname=null;
    this.ipClient =clientInfo.getUpstreamAddress();

    Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
    disp.setFilename(fileName);
    this.setDisposition(disp);
  }

  public ZipOutputRepresentation(List<Reference> referencesSource, ClientInfo clientInfo, Context context,
      String fileName, Hashtable<String, String> ListNameSource) {
    super(MediaType.APPLICATION_ZIP);
    references = referencesSource;
    this.clientInfo = clientInfo;
    this.context = context;
    this.Listname=ListNameSource;

    Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
    disp.setFilename(fileName);
    this.setDisposition(disp);
//    this.ipClient =clientInfo.getAddress();
    this.ipClient =clientInfo.getUpstreamAddress();
  }
  /**
   * 
   * @param refListAPI
   * @param clientInfo
   * @param context
   * @param fileName
   */
  public ZipOutputRepresentation(ListReferencesAPI refListAPI, ClientInfo clientInfo, Context context, String fileName) {
    super(MediaType.APPLICATION_ZIP);
    references = refListAPI.getReferencesSource();
    refMap = refListAPI.getRefSourceTarget();
    this.clientInfo = clientInfo;
    this.context = context;
    this.ipClient =clientInfo.getUpstreamAddress();

    Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
    disp.setFilename(fileName);
    this.setDisposition(disp);
  }
  
  

  @Override
  public void write(OutputStream outputStream) throws IOException {

    try {
      // create a new ZipOutputStream

      ZipOutputStream zipOutput = new ZipOutputStream(outputStream);
      // loop through the References
      String DatasetResLog =null;
      Date myDateStart = new Date();
      DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy  HH'h'mm:ss");
      DatasetResLog ="Zip in progress for user : "+"guest"+" ip : "+ipClient+"\n"+references.size()+" files requested\n"+"Start : "+dateFormat.format(myDateStart)+"\n";
      context.getLogger().log(Level.INFO,DatasetResLog);
      DatasetResLog ="List of files added :\n";
      int totalSize=0;
      long totalArchiveSize = 0;
      int buffersize = 1024;
      byte[] buf = new byte[buffersize];
      for (Reference reference : references) {
        if (reference.getLastSegment() != null) {
          try {
            context.getLogger().log(Level.FINE, "WRITE : " + reference + " into zip file");
            // try to get the file
            Representation repr = OrderResourceUtils.getFile(reference, clientInfo, context);
            // create a new ZipEntry with the name of the entry
            String[] pathPrase =reference.getPath().split("/");
            int m=0;
            for(String a : pathPrase){
              context.getLogger().log(Level.SEVERE ,"------------ DANS ZIP : pathphrase["+m+"] = "+pathPrase[m]);
              m++;
            }
            
            //identify the sunum
            String sunum =pathPrase[4].substring(1);
            context.getLogger().log(Level.SEVERE ,"------------ DANS ZIP : sunum = "+sunum);
            
            for (Listname.Entry mapentry : map.entrySet()) {
              System.out.println("clé: "+mapentry.getKey() 
                                 + " | valeur: " + mapentry.getValue());
           }
            
            ZipEntry zipEntry=null;
            if (Listname.containsKey(sunum)){
              String zip_item_name=Listname.get(sunum).toString();
    //          System.out.println("zip item name : "+zip_item_name+"\n");
              DatasetResLog +="\t"+zip_item_name+"\n";
              zipEntry = new ZipEntry(zip_item_name);

            }
            /*
            if (refMap != null) {
              if (refMap.get(reference) != null) {
                zipEntry = new ZipEntry((String) refMap.get(reference) + "/" + reference.getLastSegment());
              }
              else {
                zipEntry = new ZipEntry(reference.getLastSegment());
              }
            }
            
            else {
              zipEntry = new ZipEntry(reference.getLastSegment());
            }
            */
            zipOutput.putNextEntry(zipEntry);
            InputStream stream = null;
            totalArchiveSize += zipEntry.getSize();

            try {
              // get the stream of the File, read it and write it to
              // the Zip stream
              stream = repr.getStream();
              int count = 0;
              while ((count = stream.read(buf, 0, buffersize)) != -1) {
                zipOutput.write(buf, 0, count);
              }

            }
            catch (Exception e) {
              context.getLogger().log(Level.INFO, "File " + reference + "canno't be found", e);
            }
            finally {
              // close the entry and the file stream
              zipOutput.closeEntry();
              if (stream != null) {
                stream.close();
              }
            }
          }
          catch (SitoolsException ex) {
            // if the file canno't be found, log the error and go to the
            // next file
            context.getLogger().log(Level.INFO, "File " + reference + "canno't be found", ex);
          }
        }
      }
      // Log the zip estimated size
      Date myDateEnd = new Date();
      DatasetResLog += "End : " + dateFormat.format(myDateEnd)+"\n";
      long interval =myDateEnd.getTime()-myDateStart.getTime();
      int millisec= (int) (interval % 1000);
      int seconds = (int) (interval / 1000) % 60 ;
      int minutes = (int) ((interval / (1000*60)) % 60);
      int hours   = (int) ((interval / (1000*60*60)) % 24);
      DatasetResLog += "Time taken  : "+hours+"h "+minutes+"min "+seconds+"sec "+millisec+"ms\n";
      DatasetResLog +="Nbr files downloaded : "+Listname.size()+"\nTotal downloaded : "+totalArchiveSize/1024+" MB\n";
      // close the zip stream
      zipOutput.close();
    }
    catch (Exception e) {
      context.getLogger().log(Level.SEVERE, e.getMessage(), e);
    }
  }
}
